<div class="kingster-top-bar">
    <div class="kingster-top-bar-background"></div>
    <div class="kingster-top-bar-container kingster-container ">
        <div class="kingster-top-bar-container-inner clearfix">
            
            <div class="kingster-top-bar-right kingster-item-pdlr">
                <ul id="kingster-top-bar-menu" class="sf-menu kingster-top-bar-menu kingster-top-bar-right-menu">
                    <li class="menu-item kingster-normal-menu"><a href="https://globalia.managebac.com/login">Portal</a></li>
                    <li class="menu-item kingster-normal-menu"><a href="http://primarylibrary.sgiaedu.org/">Library</a></li>
                </ul>
                <div class="kingster-top-bar-right-social"></div><a class="kingster-top-bar-right-button" href="#" target="_blank">ADMISSION HOTLINE (+6281999467333)</a></div>
        </div>
    </div>
</div>
<header class="kingster-header-wrap kingster-header-style-plain  kingster-style-menu-right kingster-sticky-navigation kingster-style-fixed" data-navigation-offset="75px">
    <div class="kingster-header-background"></div>
    <div class="kingster-header-container  kingster-container">
        <div class="kingster-header-container-inner clearfix">
            <div class="kingster-logo  kingster-item-pdlr">
                <div class="kingster-logo-inner">
                    <a class="" href="<?php echo e(route('dashboard')); ?>">
                        <img src="<?php echo e(asset('assets/imgs/logo.webp')); ?>" alt="" width="35px" /> 
                        <span>SEKOLAH GLOBAL INDO-ASIA</span>
                    </a>
                </div>
            </div>
            <div class="kingster-navigation kingster-item-pdlr clearfix ">
                <div class="kingster-main-menu" id="kingster-main-menu">
                    <ul id="menu-main-navigation-1" class="sf-menu">
                        <?php $__currentLoopData = $navbars; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $navbar): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="menu-item menu-item-home <?php echo e($navbar->children()->get()->count() > 0 ? 'menu-item-has-children' : ''); ?> kingster-normal-menu"><a href="<?php echo e(route('page', $navbar->slug)); ?>" class="sf-with-ul-pre"><?php echo e(ucfirst($navbar->title)); ?></a>
                                <ul class="sub-menu">
                                    <?php if($navbar->children()->get()->count() > 0): ?>
                                        <?php $__currentLoopData = $navbar->children()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $navChild): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                            
                                            <li class="menu-item" data-size="60"><a href="<?php echo e(route('page', $navChild->slug)); ?>"><?php echo e(ucfirst($navChild->title)); ?></a></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                
                                    <?php endif; ?>
                                </ul>
                            </li>                            
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                    <div class="kingster-navigation-slide-bar" id="kingster-navigation-slide-bar"></div>
                </div>
            </div>
        </div>
    </div>
</header><?php /**PATH /Users/frediansimanjuntak/Documents/Freelance/websitesgia/resources/views/layouts/partials/frontend-profile/navbar.blade.php ENDPATH**/ ?>