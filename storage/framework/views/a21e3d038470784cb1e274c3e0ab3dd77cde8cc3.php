<?php $__env->startSection('title', 'AdminSGIA - Edit News'); ?>
<?php $__env->startSection('plugins.Summernote', true); ?>
<?php $__env->startSection('plugins.Select2', true); ?>
<?php $__env->startSection('plugins.DateRangePicker', true); ?>

<?php $__env->startSection('content_header'); ?>
    
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Edit News</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?php echo e(route('admin.navbars.index')); ?>">Navbars</a></li>
            <li class="breadcrumb-item"><a href="<?php echo e(route('admin.news.index')); ?>">Contents</a></li>
            <li class="breadcrumb-item active">Edit</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
    <?php if(session('message')): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert <?php echo e(session('class')); ?> alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo e(session('message')); ?>

                </div>
            </div>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="<?php echo e(route('admin.news.update', $news)); ?>" method="POST">
                    <?php echo csrf_field(); ?>
                    <?php echo method_field('PUT'); ?>
                    
                    <?php if (isset($component)) { $__componentOriginal12132ded0f8ab072425391944b4a745b11c4c786 = $component; } ?>
<?php $component = $__env->getContainer()->make(JeroenNoten\LaravelAdminLte\Components\Form\Input::class, ['name' => 'title','label' => 'Title']); ?>
<?php $component->withName('adminlte-input'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['placeholder' => 'Enter Title','value' => ''.e($news->title).'']); ?>
<?php if (isset($__componentOriginal12132ded0f8ab072425391944b4a745b11c4c786)): ?>
<?php $component = $__componentOriginal12132ded0f8ab072425391944b4a745b11c4c786; ?>
<?php unset($__componentOriginal12132ded0f8ab072425391944b4a745b11c4c786); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
                    
                    
                    
                    <?php if (isset($component)) { $__componentOriginal2b895ae1c3d1f37b9d57918b22f92b35a2a782a5 = $component; } ?>
<?php $component = $__env->getContainer()->make(JeroenNoten\LaravelAdminLte\Components\Form\Select2::class, ['name' => 'content_type','label' => 'Content Type']); ?>
<?php $component->withName('adminlte-select2'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
                        <option value="text" <?php echo e($news->content_type == 'text' ? 'selected': ''); ?>>Text</option>
                        <option value="image" <?php echo e($news->content_type == 'image' ? 'selected': ''); ?>>Image</option>
                        <option value="file" <?php echo e($news->content_type == 'file' ? 'selected': ''); ?>>File</option>
                        <option value="link" <?php echo e($news->content_type == 'link' ? 'selected': ''); ?>>Link</option>
                     <?php if (isset($__componentOriginal2b895ae1c3d1f37b9d57918b22f92b35a2a782a5)): ?>
<?php $component = $__componentOriginal2b895ae1c3d1f37b9d57918b22f92b35a2a782a5; ?>
<?php unset($__componentOriginal2b895ae1c3d1f37b9d57918b22f92b35a2a782a5); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
                    
                    <?php if (isset($component)) { $__componentOriginale27884c25ff859b6985b008c753d79ac7a5d6a0d = $component; } ?>
<?php $component = $__env->getContainer()->make(JeroenNoten\LaravelAdminLte\Components\Form\TextEditor::class, ['label' => 'Content','name' => 'content']); ?>
<?php $component->withName('adminlte-text-editor'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
                        <?php echo $news->content; ?>

                     <?php if (isset($__componentOriginale27884c25ff859b6985b008c753d79ac7a5d6a0d)): ?>
<?php $component = $__componentOriginale27884c25ff859b6985b008c753d79ac7a5d6a0d; ?>
<?php unset($__componentOriginale27884c25ff859b6985b008c753d79ac7a5d6a0d); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
                    <?php
                    $config = [
                        "singleDatePicker" => true,
                        "showDropdowns" => true,
                        "startDate" => "js:moment()",
                        "minYear" => 2000,
                        "maxYear" => "js:parseInt(moment().format('YYYY'),10)",
                        "timePicker" => true,
                        "timePicker24Hour" => true,
                        "timePickerSeconds" => true,
                        "cancelButtonClasses" => "btn-danger",
                        "locale" => ["format" => "YYYY-MM-DD HH:mm:ss"],
                    ];
                    ?>
                    <?php if (isset($component)) { $__componentOriginal533dae895851e7fa774245f7511cc5528244f395 = $component; } ?>
<?php $component = $__env->getContainer()->make(JeroenNoten\LaravelAdminLte\Components\Form\DateRange::class, ['name' => 'published_at','label' => 'Publish at','config' => $config]); ?>
<?php $component->withName('adminlte-date-range'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['value' => ''.e($news->published_at).'']); ?>
                         <?php $__env->slot('appendSlot', null, []); ?> 
                            <div class="input-group-text bg-dark">
                                <i class="fas fa-calendar-day"></i>
                            </div>
                         <?php $__env->endSlot(); ?>
                     <?php if (isset($__componentOriginal533dae895851e7fa774245f7511cc5528244f395)): ?>
<?php $component = $__componentOriginal533dae895851e7fa774245f7511cc5528244f395; ?>
<?php unset($__componentOriginal533dae895851e7fa774245f7511cc5528244f395); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
                    <div class="text-right">
                        <?php if (isset($component)) { $__componentOriginalc48319333d07a1f51a4b3e3733b4d97fe3fcdda3 = $component; } ?>
<?php $component = $__env->getContainer()->make(JeroenNoten\LaravelAdminLte\Components\Form\Button::class, ['type' => 'submit','label' => 'Submit','theme' => 'success','icon' => 'fas fa-lg fa-save']); ?>
<?php $component->withName('adminlte-button'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['class' => 'btn-flat']); ?>
<?php if (isset($__componentOriginalc48319333d07a1f51a4b3e3733b4d97fe3fcdda3)): ?>
<?php $component = $__componentOriginalc48319333d07a1f51a4b3e3733b4d97fe3fcdda3; ?>
<?php unset($__componentOriginalc48319333d07a1f51a4b3e3733b4d97fe3fcdda3); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
                    </div>
                </form> 
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        $(document).ready(function(){
            $("select[name='content_type']").on('change', function() {
                console.log(this.value);
            });;
            
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlte::page', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/frediansimanjuntak/Documents/Freelance/websitesgia/resources/views/admin-profile/news/edit.blade.php ENDPATH**/ ?>