<?php $__env->startSection('title', 'AdminSGIA - Navbar Detail'); ?>
<?php $__env->startSection('plugins.Datatables', true); ?>


<?php
$heads = [
    'ID',
    'Title',
    ['label' => 'Positions', 'no-export' => true, 'width' => 5],
    ['label' => 'Actions', 'no-export' => true, 'width' => 5],
];
$data = array();
foreach ($navbarChild as $navbarC) {
    $seqUp = (int)$navbarC->sequence-1;
    $seqDown = (int)$navbarC->sequence+1;
    $btnEdit = '<button class="btn btn-xs btn-default text-primary mx-1 shadow" title="Edit" data-toggle="modal" data-target="#modalEditNavbar'.$navbarC->id.'">
                    <i class="fa fa-lg fa-fw fa-pen"></i>
                </button>';
    $btnDelete = '<button class="btn btn-xs btn-default text-danger mx-1 shadow" title="Delete">
                    <i class="fa fa-lg fa-fw fa-trash"></i>
                </button>';
    // $btnDetails = '<button class="btn btn-xs btn-default text-teal mx-1 shadow" title="Details">
    //                 <i class="fa fa-lg fa-fw fa-eye"></i>
    //             </button>';
    $btnMoveUp = '<button class="btn btn-xs btn-default text-teal mx-1 shadow btn-move-up" title="Details" data-sequence="'.$seqUp.'" data-navbar="'.$navbarC->id.'">
                    <i class="fas fa-lg fa-fw fa-chevron-up"></i>
                </button>';
    $btnMoveDown = '<button class="btn btn-xs btn-default text-teal mx-1 shadow btn-move-down" title="Details" data-sequence="'.$seqDown.'" data-navbar="'.$navbarC->id.'">
                    <i class="fas fa-lg fa-fw fa-chevron-down"></i>
                </button>';
    $formUpdateNavbar = '<form id="updateNavbar_'.$navbarC->id.'" action='.route("admin.navbars.update", $navbarC).' method="POST">
                        '.csrf_field().'
                        <input type="hidden" name="_method" value="PUT" />
                        <input type="hidden" name="sequence"/>
                        <input type="hidden" name="parent_id" value="'.$navbarC->parent_id.'">
                        <input type="hidden" name="title" value="'.$navbarC->title.'">
                    </form>';
    $modalEdit = '<div class="modal fade" id="modalEditNavbar'.$navbarC->id.'" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
                        <div class="modal-content">
                            <div class="modal-header bg-teal">
                                <h4 class="modal-title">
                                    <i class="fas fa-plus mr-2"></i> Edit Navbar            
                                </h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <form action='.route("admin.navbars.update", $navbarC).' method="POST" id="formEditNavbar'.$navbarC->id.'">
                                <div class="modal-body">
                                    '.csrf_field().'
                                    <input type="hidden" name="_method" value="PUT" />
                                    <input type="hidden" name="sequence" value="'.$navbarC->sequence.'">
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label for="title"> Title </label>
                                            <div class="input-group">
                                                <input id="title" name="title" class="form-control" placeholder="Enter Title" value="'.$navbarC->title.'">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="modal-footer">
                                    <button type="submit" class="btn btn-success mr-auto submit-edit-navbar"> Save </button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal"> Discard </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>';
    $bar = array(
        $navbarC->sequence, 
        $navbarC->title,
        '<nobr>'.$btnMoveUp.$btnMoveDown.$formUpdateNavbar.'</nobr>',
        '<nobr>'.$btnEdit.$btnDelete.$modalEdit.'</nobr>'
    );
    $data[] = $bar;
}


$config = [
    'data' => $data,
    'order' => [[1, 'asc']],
    'columns' => [null, null, ['orderable' => false]],
];
// dd($config['data']);
?>

<?php $__env->startSection('content_header'); ?>
    
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Navbars</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?php echo e(route('admin.navbars.index')); ?>">Navbars</a></li>
            <li class="breadcrumb-item active"><?php echo e($navbar->title); ?></li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
    <?php if(session('message')): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert <?php echo e(session('class')); ?> alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo e(session('message')); ?>

                </div>
            </div>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Navbar List | Head : <?php echo e($navbar->title); ?></h3>
                    <div class="card-tools">
                        <?php if (isset($component)) { $__componentOriginalc48319333d07a1f51a4b3e3733b4d97fe3fcdda3 = $component; } ?>
<?php $component = $__env->getContainer()->make(JeroenNoten\LaravelAdminLte\Components\Form\Button::class, ['label' => 'Add Navbar']); ?>
<?php $component->withName('adminlte-button'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['data-toggle' => 'modal','data-target' => '#modalCreateNavbar','class' => 'bg-teal']); ?>
<?php if (isset($__componentOriginalc48319333d07a1f51a4b3e3733b4d97fe3fcdda3)): ?>
<?php $component = $__componentOriginalc48319333d07a1f51a4b3e3733b4d97fe3fcdda3; ?>
<?php unset($__componentOriginalc48319333d07a1f51a4b3e3733b4d97fe3fcdda3); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
                    </div>
                </div>
                <!-- ./card-header -->
                <div class="card-body">
                    <?php if (isset($component)) { $__componentOriginal5c3a95af394031b1bfa79f2f00cd6019494000a3 = $component; } ?>
<?php $component = $__env->getContainer()->make(JeroenNoten\LaravelAdminLte\Components\Tool\Datatable::class, ['id' => 'table1','heads' => $heads]); ?>
<?php $component->withName('adminlte-datatable'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
                        <?php $__currentLoopData = $config['data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <?php $__currentLoopData = $row; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cell): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <td><?php echo $cell; ?></td>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php if (isset($__componentOriginal5c3a95af394031b1bfa79f2f00cd6019494000a3)): ?>
<?php $component = $__componentOriginal5c3a95af394031b1bfa79f2f00cd6019494000a3; ?>
<?php unset($__componentOriginal5c3a95af394031b1bfa79f2f00cd6019494000a3); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
                    
                    <?php if (isset($component)) { $__componentOriginal3170ce38bba9a254ea7cdfc3b7aa9def8f17c1f0 = $component; } ?>
<?php $component = $__env->getContainer()->make(JeroenNoten\LaravelAdminLte\Components\Tool\Modal::class, ['id' => 'modalCreateNavbar','title' => 'Add New Navbar','size' => 'lg','theme' => 'teal','icon' => 'fas fa-plus','vCentered' => true,'staticBackdrop' => true,'scrollable' => true]); ?>
<?php $component->withName('adminlte-modal'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
                        <form action="<?php echo e(route('admin.navbars.store')); ?>" method="POST" id="formCreateNavbar">
                            <?php echo e(csrf_field()); ?>

                            <div class="row">
                                <input type="hidden" name="parent_id" value="<?php echo e($navbar->id); ?>">
                                <?php if (isset($component)) { $__componentOriginal12132ded0f8ab072425391944b4a745b11c4c786 = $component; } ?>
<?php $component = $__env->getContainer()->make(JeroenNoten\LaravelAdminLte\Components\Form\Input::class, ['name' => 'title','label' => 'Title','fgroupClass' => 'col-md-12','disableFeedback' => true]); ?>
<?php $component->withName('adminlte-input'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['placeholder' => 'Enter Title']); ?>
<?php if (isset($__componentOriginal12132ded0f8ab072425391944b4a745b11c4c786)): ?>
<?php $component = $__componentOriginal12132ded0f8ab072425391944b4a745b11c4c786; ?>
<?php unset($__componentOriginal12132ded0f8ab072425391944b4a745b11c4c786); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
                            </div>
                        </form>
                         <?php $__env->slot('footerSlot', null, []); ?> 
                            <?php if (isset($component)) { $__componentOriginalc48319333d07a1f51a4b3e3733b4d97fe3fcdda3 = $component; } ?>
<?php $component = $__env->getContainer()->make(JeroenNoten\LaravelAdminLte\Components\Form\Button::class, ['theme' => 'success','type' => 'submit','label' => 'Save']); ?>
<?php $component->withName('adminlte-button'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['class' => 'mr-auto submit-create-navbar']); ?>
<?php if (isset($__componentOriginalc48319333d07a1f51a4b3e3733b4d97fe3fcdda3)): ?>
<?php $component = $__componentOriginalc48319333d07a1f51a4b3e3733b4d97fe3fcdda3; ?>
<?php unset($__componentOriginalc48319333d07a1f51a4b3e3733b4d97fe3fcdda3); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
                            <?php if (isset($component)) { $__componentOriginalc48319333d07a1f51a4b3e3733b4d97fe3fcdda3 = $component; } ?>
<?php $component = $__env->getContainer()->make(JeroenNoten\LaravelAdminLte\Components\Form\Button::class, ['theme' => 'danger','label' => 'Discard']); ?>
<?php $component->withName('adminlte-button'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes(['data-dismiss' => 'modal']); ?>
<?php if (isset($__componentOriginalc48319333d07a1f51a4b3e3733b4d97fe3fcdda3)): ?>
<?php $component = $__componentOriginalc48319333d07a1f51a4b3e3733b4d97fe3fcdda3; ?>
<?php unset($__componentOriginalc48319333d07a1f51a4b3e3733b4d97fe3fcdda3); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
                         <?php $__env->endSlot(); ?>
                     <?php if (isset($__componentOriginal3170ce38bba9a254ea7cdfc3b7aa9def8f17c1f0)): ?>
<?php $component = $__componentOriginal3170ce38bba9a254ea7cdfc3b7aa9def8f17c1f0; ?>
<?php unset($__componentOriginal3170ce38bba9a254ea7cdfc3b7aa9def8f17c1f0); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        $(document).ready(function(){
            $(".submit-create-navbar").click(function(){        
                $("#formCreateNavbar").submit(); // Submit the form
            });
            $(".btn-move-up").click(function(){      
                var position = $(this).attr('data-sequence');
                var navbarId = $(this).attr('data-navbar');
                $("input[name='sequence']").val(position);
                $('#updateNavbar_'+navbarId).submit();
            });
            $(".btn-move-down").click(function(){      
                var position = $(this).attr('data-sequence');
                var navbarId = $(this).attr('data-navbar');
                console.log(navbarId, position);
                $("input[name='sequence']").val(position);
                $('#updateNavbar_'+navbarId).submit();
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlte::page', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/frediansimanjuntak/Documents/Freelance/websitesgia/resources/views/admin-profile/navbars/show.blade.php ENDPATH**/ ?>