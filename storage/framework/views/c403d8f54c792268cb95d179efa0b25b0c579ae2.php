<?php $__env->startSection("content"); ?>

<section class="mb-4">
    <div class="container-fluid actwrap">
        <div class="row">
            <div class="col-12">
                <h4>Candidate Student</h4>
                <p>Candidate Student Data</p>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid actwrap">
    <div class="row">
        <div class="col-lg-12">
          <div class="card card-table nbs mt-4">

            <div class="d-flex flex-row w-100 align-items-end py-3 px-4 mb-4">
              <div class="px-2">
                <label for="">Search Candidate by Name</label>
                <input type="text" name="" value="" class="form-control" placeholder="Enter Candidate by Name">
              </div>

              <div class="px-2">
                <label for="">Search by Grade</label>
                <select class="form-control" name="">
                    <option value="" selected disabled>-- Select Grade --</option>
                    <option value=""></option>
                    <option value=""></option>
                </select>
              </div>

              <div class="px-2">
                <label for="">Search by Status</label>
                <select class="form-control" name="">
                    <option value="" selected disabled>-- Select Status --</option>
                    <option value=""></option>
                    <option value=""></option>
                </select>
              </div>
              <div class="px-2">
                <button href="#" class="input-group-text btn bg-primary text-white"><i class="uil uil-search"></i> Search</button>
              </div>
            </div>

            <div class="table-responsive">
              <table class="table table-stripped">
                <thead>
                  <th>#</th>
                  <th>Student Name</th>
                  <th>Religion</th>
                  <th>Gender</th>
                  <th>Grade</th>
                  <th>Father</th>
                  <th>Phone</th>
                  <th>Mother</th>
                  <th>Phone</th>
                  <th>Details</th>
                  <th>Action</th>
                </thead>
                <tbody>
                  <?php for ($i=1; $i < 2 ; $i++) {
                   ?>
                  <tr>
                    <td>
                      <p><?php echo($i) ?></p>
                    </td>
                    <td>
                      <p>Eugene Arief Vianata</p>
                    </td>
                    <td>
                      <p>Islam</p>
                    </td>
                    <td>
                      <p>M</p>
                    </td>
                    <td>
                      <p>Grade 12</p>
                    </td>
                    <td>
                      <p>Budi</p>
                    </td>
                    <td>
                      <p>0853123456789</p>
                    </td>
                    <td>
                      <p>Ani</p>
                    </td>
                    <td>
                      <p>0854122344578</p>
                    </td>
                    <td>
                        <div class="d-flex flex-row">
                          <p class="mx-1"><a href="<?php echo e(url('/editcandidate')); ?>" class="nbb"><button type="button" name="button" class="btn btn-warning bg-warning">Edit</button></a></p>
                          <p class="mx-1"><a href="<?php echo e(url('/detailscandidate')); ?>" class="nbb"><button type="button" name="button" class="btn btn-info bg-info">Details</button></a></p>
                        </div>
                    </td>
                    <td>
                      <p><button type="button" name="button" class="btn btn-primary bg-primary">Confirm</button></p>
                    </td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>

            <div class="d-flex flex-row px-4 py-3 align-items-center">
              <div class="me-auto">
                <div class="input-group">
                  <label for="" class="input-group-text nbg">Show Data</label>
                  <select class="form-control" name="">
                    <option value="">5</option>
                    <option value="">10</option>
                    <option value="">50</option>
                  </select>
                </div>
              </div>
              <div class="ms-auto">
                <nav aria-label="Page navigation" class="act-pagination">
                  <ul class="pagination justify-content-end">
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true"><i class="uil uil-angle-left-b"></i></span>
                      </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true"><i class="uil uil-angle-right-b"></i></span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('js'); ?>
  <script type="text/javascript">
      $('#studentsCollapse').addClass('collapse show');
  </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make("admin.layouts.app", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/frediansimanjuntak/Documents/Freelance/websitesgia/resources/views/admin/students/candidate.blade.php ENDPATH**/ ?>