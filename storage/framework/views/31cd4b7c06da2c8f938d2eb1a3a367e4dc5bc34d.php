<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- fonts Rubik-->
    <link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link href="<?php echo e(asset('assets/css/bootstrap/bootstrap.min.css')); ?>" rel="stylesheet">
    <!-- CSS -->
    <link href="<?php echo e(asset('assets/css/main.css')); ?>" rel="stylesheet">
    <!-- unicons -->
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v3.0.6/css/line.css">

    <title>International School Batam | Batam Kota | Sekolah Global Indo - Asia</title>

    <?php echo $__env->yieldContent("css"); ?>
  </head>
  <body>
    <div id="loading">
    </div>
    <div id="SGIA">
      <!-- Side Menu -->
      <!-- Model 1 -->
      <div class="navbar bigicon" id="sidemenu">
        <div class="logo-brand">
          <a href="<?php echo e(url('/')); ?>"><img src="<?php echo e(asset('assets/imgs/logo-white.png')); ?>" alt="" class="logo"></a>
        </div>
        <div class="menus">
          <ul>
            <li id="dashboardPage">
              <p class="mb-0"><a href="<?php echo e(url('/')); ?>" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> Dashboard</a></p>
            </li>
            <li class="collapse-menu">
              <button class="btn collapsed link" data-bs-toggle="collapse" data-bs-target="#admissionCollapse" role="button" aria-expanded="false" aria-controls="admissionCollapse"><img src="<?php echo e(asset('assets/imgs/icon/studies.svg')); ?>" alt="" class="icon"> Admission</button>
            </li>
            <li class="collapse-menu">
              <button class="btn collapsed link" data-bs-toggle="collapse" data-bs-target="#studentsCollapse" role="button" aria-expanded="false" aria-controls="studentsCollapse"><img src="<?php echo e(asset('assets/imgs/icon/student.svg')); ?>" alt="" class="icon"> Students</button>
            </li>

            <li class="collapse-menu">
              <button class="btn collapsed link" data-bs-toggle="collapse" data-bs-target="#reportsCollapse" role="button" aria-expanded="false" aria-controls="reportsCollapse"><img src="<?php echo e(asset('assets/imgs/icon/summary.svg')); ?>" alt="" class="icon"> Reports</button>
            </li>

            <li class="collapse-menu">
              <button class="btn collapsed link" data-bs-toggle="collapse" data-bs-target="#feesCollapse" role="button" aria-expanded="false" aria-controls="feesCollapse"><img src="<?php echo e(asset('assets/imgs/icon/fee.svg')); ?>" alt="" class="icon"> Fees</button>
            </li>

            <li class="collapse-menu">
              <button class="btn collapsed link" data-bs-toggle="collapse" data-bs-target="#financeCollapse" role="button" aria-expanded="false" aria-controls="financeCollapse"><img src="<?php echo e(asset('assets/imgs/icon/finance.svg')); ?>" alt="" class="icon"> Finances</button>
            </li>

            <li>
              <p class="mb-0"><a href="#" class="link"><img src="<?php echo e(asset('assets/imgs/icon/logout.svg')); ?>" alt="" class="icon"> Logout</a></p>
            </li>

          </ul>
        </div>
      </div>

      <!-- Model 2 -->

      <!-- <div class="navbar" id="sidemenu">
        <div class="logo-brand">
          <a href="<?php echo e(url('/')); ?>"><img src="<?php echo e(asset('assets/imgs/logo-white.png')); ?>" alt="" class="logo"></a>
        </div>
        <div class="menus">
          <ul>
            <li id="dashboardPage">
              <p class="mb-0"><a href="<?php echo e(url('/')); ?>"><i class="uil uil-university"></i> Dashboard</a></p>
            </li>
            <li class="collapse-menu">
              <button class="btn collapsed" data-bs-toggle="collapse" href="#admissionCollapse" role="button" aria-expanded="false" aria-controls="admissionCollapse"><i class="uil uil-users-alt"></i> Admission</button>
              <ul id="admissionCollapse" class="collapse collapse-menu">
                  <li>
                      <a href="#"><i class="uil uil-percentage"></i> Siblings Discount</a>
                  </li>
                  <li class="<?php echo e(Request::segment(1) === 'grade' ? 'active' : null); ?>">
                      <a href="<?php echo e(url('grade')); ?>"><i class="uil uil-graduation-cap"></i> Grade</a>
                  </li>
                  <li class="<?php echo e(Request::segment(1) === 'school-years' ? 'active' : null); ?>">
                      <a href="<?php echo e(url('school-years')); ?>"><i class="uil uil-calendar-alt"></i> School Years</a>
                  </li>
                  <li class="<?php echo e(Request::segment(1) === 'nationality' ? 'active' : null); ?>">
                      <a href="<?php echo e(url('nationality')); ?>"><i class="uil uil-graduation-cap"></i> Nationality</a>
                  </li>
                  <li>
                      <a href="#"><i class="uil uil-calendar-alt"></i> Discount Type</a>
                  </li>
              </ul>
            </li>

            <li class="collapse-menu">
              <button class="btn collapsed" data-bs-toggle="collapse" href="#studentsCollapse" role="button" aria-expanded="false" aria-controls="studentsCollapse"><i class="uil uil-graduation-cap"></i> Students</button>
              <ul id="studentsCollapse" class="collapse collapse-menu">
                  <li class="<?php echo e(Request::segment(1) === 'addcandidate' ? 'active' : null); ?>">
                      <a href="<?php echo e(url('/addcandidate')); ?>"><i class="uil uil-percentage"></i> Add Candidate Student</a>
                  </li>
                  <li class="<?php echo e(Request::segment(1) === 'candidate' ? 'active' : null); ?>">
                      <a href="<?php echo e(url('/candidate')); ?>"><i class="uil uil-graduation-cap"></i> Candidate Student</a>
                  </li>
                  <li class="<?php echo e(Request::segment(1) === 'student-information' ? 'active' : null); ?>">
                      <a href="<?php echo e(url('/student-information')); ?>"><i class="uil uil-calendar-alt"></i> Student Information</a>
                  </li>
                  <li>
                      <a href="#"><i class="uil uil-calendar-alt"></i> Active Students</a>
                  </li>
                  <li>
                      <a href="#"><i class="uil uil-calendar-alt"></i> Students Summary</a>
                  </li>
              </ul>
            </li>

            <li class="collapse-menu">
              <button class="btn collapsed" data-bs-toggle="collapse" href="#reportsCollapse" role="button" aria-expanded="false" aria-controls="reportsCollapse"><i class="uil uil-document-info"></i> Reports</button>
              <ul id="reportsCollapse" class="collapse collapse-menu">
                  <li>
                      <a href="#"><i class="uil uil-percentage"></i> <span>History Candidate Report</span></a>
                  </li>
                  <li>
                      <a href="#"><i class="uil uil-graduation-cap"></i> History Report</a>
                  </li>
                  <li>
                      <a href="#"><i class="uil uil-calendar-alt"></i> Advance Report</a>
                  </li>
              </ul>
            </li>

            <li class="collapse-menu">
              <button class="btn collapsed" data-bs-toggle="collapse" href="#feesCollapse" role="button" aria-expanded="false" aria-controls="feesCollapse"><i class="uil uil-document-info"></i> Fees</button>
              <ul id="feesCollapse" class="collapse collapse-menu">
                  <li>
                      <a href="#"><i class="uil uil-percentage"></i> <span>Set School Fees</span></a>
                  </li>
                  <li>
                    <a href="#"><i class="uil uil-calendar-alt"></i> Maintain School Fees</a>
                  </li>
                  <li>
                      <a href="#"><i class="uil uil-graduation-cap"></i> Set Levy Fees</a>
                  </li>
                  <li>
                    <a href="#"><i class="uil uil-calendar-alt"></i> Maintain Levy Fees</a>
                  </li>
              </ul>
            </li>

            <li class="collapse-menu">
              <button class="btn collapsed" data-bs-toggle="collapse" href="#financeCollapse" role="button" aria-expanded="false" aria-controls="financeCollapse"><i class="uil uil-document-info"></i> Finances</button>
              <ul id="financeCollapse" class="collapse collapse-menu">
                  <li>
                      <a href="#"><i class="uil uil-percentage"></i> <span>School Fees</span></a>
                  </li>
                  <li>
                    <a href="#"><i class="uil uil-calendar-alt"></i> History Payment School Fee</a>
                  </li>
                  <li>
                      <a href="#"><i class="uil uil-graduation-cap"></i> Approval School Fee</a>
                  </li>
                  <li>
                    <a href="#"><i class="uil uil-calendar-alt"></i> Levy Fee</a>
                  </li>
                  <li>
                    <a href="#"><i class="uil uil-calendar-alt"></i> History Payment Levy Fee</a>
                  </li>
                  <li>
                      <a href="#"><i class="uil uil-graduation-cap"></i> Approval Levy Fee</a>
                  </li>
              </ul>
            </li>

            <li>
              <p class="mb-0"><a href="#"><i class="uil uil-sign-out-alt"></i> Logout</a></p>
            </li>

          </ul>
        </div>
      </div> -->

      <div class="menu-float" id="menu-float">
        <ul id="admissionCollapse" class="collapse collapse-menu" data-bs-parent="#menu-float">
          <li>
            <a href="#" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> Siblings Discount</a>
          </li>
          <li class="<?php echo e(Request::segment(1) === 'grade' ? 'active' : null); ?>">
            <a href="<?php echo e(url('grade')); ?>" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> Grade</a>
          </li>
          <li class="<?php echo e(Request::segment(1) === 'school-years' ? 'active' : null); ?>">
            <a href="<?php echo e(url('school-years')); ?>" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> School Years</a>
          </li>
          <li class="<?php echo e(Request::segment(1) === 'nationality' ? 'active' : null); ?>">
            <a href="<?php echo e(url('nationality')); ?>" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> Nationality</a>
          </li>
          <li>
            <a href="#" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> Discount Type</a>
          </li>
        </ul>
        <ul id="studentsCollapse" class="collapse collapse-menu" data-bs-parent="#menu-float">
            <li class="<?php echo e(Request::segment(1) === 'addcandidate' ? 'active' : null); ?>">
                <a href="<?php echo e(url('/addcandidate')); ?>" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> Add Candidate Student</a>
            </li>
            <li class="<?php echo e(Request::segment(1) === 'candidate' ? 'active' : null); ?>">
                <a href="<?php echo e(url('/candidate')); ?>" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> Candidate Student</a>
            </li>
            <li class="<?php echo e(Request::segment(1) === 'student-information' ? 'active' : null); ?>">
                <a href="<?php echo e(url('/student-information')); ?>" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> Student Information</a>
            </li>
            <li>
                <a href="#" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> Active Students</a>
            </li>
            <li>
                <a href="#" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> Students Summary</a>
            </li>
        </ul>
        <ul id="reportsCollapse" class="collapse collapse-menu" data-bs-parent="#menu-float">
            <li>
                <a href="#" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> <span>History Candidate Report</span></a>
            </li>
            <li>
                <a href="#" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> History Report</a>
            </li>
            <li>
                <a href="#" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> Advance Report</a>
            </li>
        </ul>
        <ul id="feesCollapse" class="collapse collapse-menu" data-bs-parent="#menu-float">
            <li>
                <a href="#" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> <span>Set School Fees</span></a>
            </li>
            <li>
              <a href="#" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> Maintain School Fees</a>
            </li>
            <li>
                <a href="#" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> Set Levy Fees</a>
            </li>
            <li>
              <a href="#" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> Maintain Levy Fees</a>
            </li>
        </ul>
        <ul id="financeCollapse" class="collapse collapse-menu" data-bs-parent="#menu-float">
            <li>
                <a href="#" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> <span>School Fees</span></a>
            </li>
            <li>
              <a href="#" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> History Payment School Fee</a>
            </li>
            <li>
                <a href="#" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> Approval School Fee</a>
            </li>
            <li>
              <a href="#" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> Levy Fee</a>
            </li>
            <li>
              <a href="#" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> History Payment Levy Fee</a>
            </li>
            <li>
                <a href="#" class="link"><img src="<?php echo e(asset('assets/imgs/icon/computer.svg')); ?>" alt="" class="icon"> Approval Levy Fee</a>
            </li>
        </ul>
      </div>

      <!-- Main Menu -->
      <div id="mainmenu" class="bigicon">
        <div class="">
          <div class="usermenu d-flex flex-row align-items-center justify-content-start">
            <div class="px-1">
              <button type="button" name="button" class="btn" id="hamburger"><i class="uil uil-bars"></i></button>
            </div>
            <div class="px-1">
                <img src="<?php echo e(asset('assets/imgs/logo.webp')); ?>" alt="" width="50px">
            </div>
            <div class="px-1 ms-3">
              <h3 class="mb-0">Sekolah Global Indo - Asia</h3>
              <h5 class="mb-0">Education for The Future</h5>
            </div>
            <div class="px-1 ms-auto me-3">
              <button type="button" name="button" class="btn"><i class="uil uil-sign-out-alt"></i> Logout</button>
            </div>
          </div>
        </div>
        <div class="contentArea">
          <?php echo $__env->yieldContent("content"); ?>
        </div>
        <footer id="footer">
          <div class="footer text-secondary">
            <div class="row justify-content-between align-items-center">
              <div class="col">
                <p class="mb-0">Jln. Raya Batam Center, Kav.SGIA</p>
                <p class="mb-0">Batam Center - 29400 Batam - Indonesia</p>
              </div>
              <div class="col text-lg-end">
                <p class="mb-0">copyright © SGIA 2021</p>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="<?php echo e(asset('assets/js/bootstrap/bootstrap.bundle.min.js')); ?>"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="crossorigin="anonymous"></script>
    <!-- chart js -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.5.0/dist/chart.min.js"></script>
    <script type="text/javascript">
      $('#hamburger').click(function(){
        $('#sidemenu').toggleClass('minimize');
        $('.menu-float').toggleClass('minimize');
        $('#mainmenu').toggleClass('w-100');
      });
    </script>
    <script type="text/javascript">
        $(document).ready(setTimeout(function() {
             $('#loading').fadeOut();// or fade, css display however you'd like.
        }, 600));
    </script>
    <script type="text/javascript">

    </script>
    <?php echo $__env->yieldPushContent("js"); ?>
  </body>
</html>
<?php /**PATH /Users/frediansimanjuntak/Documents/Freelance/websitesgia/resources/views/admin/layouts/app.blade.php ENDPATH**/ ?>