<?php $__env->startSection('content'); ?>
    <?php if($navbarContent): ?>
        <div class="kingster-page-wrapper" id="kingster-page-wrapper">
            <div class="kingster-blog-title-wrap  kingster-style-custom kingster-feature-image" style="background-image: url(upload/shutterstock_299633474.jpg) ;">
                <div class="kingster-header-transparent-substitute"></div>
                <div class="kingster-blog-title-overlay" style="opacity: 0.01 ;"></div>
                <div class="kingster-blog-title-bottom-overlay"></div>
                <div class="kingster-blog-title-container kingster-container">
                    <div class="kingster-blog-title-content kingster-item-pdlr" style="padding-top: 400px ;padding-bottom: 80px ;">
                        <header class="kingster-single-article-head clearfix">
                            <div class="kingster-single-article-head-right">
                                <h1 class="kingster-single-article-title"><?php echo e(ucfirst($navbar->title)); ?></h1>
                            </div>
                        </header>
                    </div>
                </div>
            </div>
            <div class="kingster-breadcrumbs">
                <div class="kingster-breadcrumbs-container kingster-container">
                    <div class="kingster-breadcrumbs-item kingster-item-pdlr"> <span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to Kingster." href="<?php echo e(route('dashboard')); ?>" class="home"><span property="name">Home</span></a>
                        <meta property="position" content="1">
                        <?php if($navbar->parent()->first()): ?>
                            </span>&gt;<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to the Blog category archives." href="<?php echo e(route('page', $navbar->parent()->first()->slug)); ?>" class="taxonomy category"><span property="name"><?php echo e(ucfirst($navbar->parent()->first()->title)); ?></span></a>
                            <meta property="position" content="2">                    
                        <?php endif; ?>
                        </span>&gt;<span property="itemListElement" typeof="ListItem"><span property="name"><?php echo e(ucfirst($navbar->title)); ?></span>
                        <meta property="position" content="3">
                        </span>
                    </div>
                </div>
            </div>
            <div class="kingster-content-container kingster-container">
                <div class=" kingster-sidebar-wrap clearfix kingster-line-height-0 kingster-sidebar-style-none">
                    <div class=" kingster-sidebar-center kingster-column-60 kingster-line-height">
                        <div class="kingster-content-wrap kingster-item-pdlr clearfix">
                            <div class="kingster-content-area">
                                <article id="post-1268" class="post-1268 post type-post status-publish format-standard has-post-thumbnail hentry category-blog category-post-format tag-news">
                                    <div class="kingster-single-article clearfix">
                                        <div class="kingster-single-article-content">
                                            <?php echo $navbarContent->content; ?>

                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </div>     
    <?php else: ?>        
        <?php echo $__env->make('layouts.partials.frontend-profile.404', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?> 
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script> console.log('Hi!'); </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend-profile', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/frediansimanjuntak/Documents/Freelance/websitesgia/resources/views/frontend-profile/pages/show.blade.php ENDPATH**/ ?>