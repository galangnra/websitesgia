<div class="kingster-mobile-header-wrap">
    <div class="kingster-mobile-header kingster-header-background kingster-style-slide kingster-sticky-mobile-navigation " id="kingster-mobile-header">
        <div class="kingster-mobile-header-container kingster-container clearfix">
            <div class="kingster-logo  kingster-item-pdlr">
                <div class="kingster-logo-inner">
                    <a class="" href="<?php echo e(route('dashboard')); ?>"><img src="<?php echo e(asset('assets/imgs/logo.webp')); ?>" alt="" width="30px"/> <span>SEKOLAH GLOBAL INDO-ASIA</span></a>
                </div>
            </div>
            <div class="kingster-mobile-menu-right">
                
                <div class="kingster-mobile-menu"><a class="kingster-mm-menu-button kingster-mobile-menu-button kingster-mobile-button-hamburger" href="#kingster-mobile-menu"><span></span></a>
                    <div class="kingster-mm-menu-wrap kingster-navigation-font" id="kingster-mobile-menu" data-slide="right">
                        <ul id="menu-main-navigation" class="m-menu">
                            <?php $__currentLoopData = $navbars; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $navbar): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li class="menu-item <?php echo e($navbar->children()->get()->count() > 0 ? 'menu-item-has-children' : ''); ?>"><a href="<?php echo e(route('page', $navbar->slug)); ?>"><?php echo e(ucfirst($navbar->title)); ?></a>
                                    <ul class="sub-menu">
                                        <?php if($navbar->children()->get()->count() > 0): ?>
                                            <?php $__currentLoopData = $navbar->children()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $navChild): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                            
                                                <li class="menu-item" data-size="60"><a href="<?php echo e(route('page', $navChild->slug)); ?>"><?php echo e(ucfirst($navChild->title)); ?></a></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                
                                        <?php endif; ?>
                                    </ul>
                                </li>                            
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?php /**PATH /Users/frediansimanjuntak/Documents/Freelance/websitesgia/resources/views/layouts/partials/frontend-profile/navbar-responsive.blade.php ENDPATH**/ ?>