<?php $__env->startSection("content"); ?>
<section class="mb-4">
    <div class="container-fluid actwrap">
        <div class="row">
            <div class="col-12">
                <h4>Hello, User</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid actwrap">
    <div class="row">
        <div class="col-lg-3">
          <a href="<?php echo e(url('/active-student')); ?>" class="nbb">
            <div class="card act-card bg-primary text-light pattern btn">
                <div class="d-flex flex-row align-items-center">
                    <div class="pe-lg-4 pe-2">
                      <h5 class="icons"><i class="uil uil-graduation-cap"></i></h5>
                    </div>
                    <div class="text-start">
                      <h6 class="desc mb-0">Total</h6>
                      <h3 class="values mb-0">461</h3>
                      <h6 class="desc">Student</h6>
                    </div>
                </div>
            </div>
            </a>
        </div>
        <div class="col-lg-3">
          <a href="<?php echo e(url('/candidate')); ?>" class="nbb">
            <div class="card act-card bg-success text-light pattern btn">
                <div class="d-flex flex-row align-items-center">
                    <div class="pe-lg-4 pe-2">
                      <h5 class="icons"><i class="uil uil-user"></i></h5>
                    </div>
                    <div class="text-start">
                      <h6 class="desc mb-0">Total</h6>
                      <h3 class="values mb-0">10</h3>
                      <h6 class="desc">Candidate Student</h6>
                    </div>
                </div>
            </div>
            </a>
        </div>
        <div class="col-lg-3">
          <a href="<?php echo e(url('/visitor')); ?>" class="nbb">
            <div class="card act-card bg-danger text-light pattern btn">
                <div class="d-flex flex-row align-items-center">
                    <div class="pe-lg-4 pe-2">
                      <h5 class="icons"><i class="uil uil-eye"></i></h5>
                    </div>
                    <div class="text-start">
                      <h6 class="desc mb-0">Total</h6>
                      <h3 class="values mb-0">25</h3>
                      <h6 class="desc">Visitor</h6>
                    </div>
                </div>
            </div>
          </a>
        </div>
        <div class="col-lg-3">
          <a href="<?php echo e(url('/family')); ?>" class="nbb">
            <div class="card act-card bg-info text-light pattern btn">
                <div class="d-flex flex-row align-items-center">
                    <div class="pe-lg-4 pe-2">
                      <h5 class="icons"><i class="uil uil-users-alt"></i></h5>
                    </div>
                    <div class="text-start">
                      <h6 class="desc mb-0">Total</h6>
                      <h3 class="values mb-0">461</h3>
                      <h6 class="desc">Family</h6>
                    </div>
                </div>
            </div>
            </a>
        </div>
    </div>

    <div class="row my-4">
        <div class="col-lg-6">
          <h5 class="title">Total Students</h5>
          <div class="card act-card">
            <canvas id="myChart" width="100%" height="55" class="act-chart"></canvas>
          </div>
        </div>

        <div class="col-lg-6">
          <h5 class="title">Global Statistic</h5>
          <div class="card act-card">
            <canvas id="myChart2" width="100%" height="55" class="act-chart"></canvas>
          </div>
        </div>
    </div>

    <div class="row my-4">
      <h5 class="title">Active Student</h5>
        <div class="col-lg-12">
          <div class="card card-table">
            <div class="d-flex flex-row py-3 px-4 text-dark">
              <h5>Pre - K</h5>
              <h5 class="ms-auto text-primary">Total Student : 461</h5>
            </div>
            <div class="table-responsive">
              <table class="table table-stripped">
                <thead>
                  <th>#</th>
                  <th>ID</th>
                  <th>Student Name</th>
                  <th>NISN</th>
                  <th>Nationality</th>
                  <th>Religion</th>
                  <th>Gender</th>
                  <th>OnGoingClass</th>
                  <th>ClassDif</th>
                </thead>
                <tbody>
                  <?php for ($i=1; $i < 11 ; $i++) {
                   ?>
                  <tr>
                    <td>
                      <p><?php echo($i) ?></p>
                    </td>
                    <td>
                      <p>1601000173</p>
                    </td>
                    <td>
                      <p>Fressia Wisely Fang</p>
                    </td>
                    <td>
                      <p></p>
                    </td>
                    <td>
                      <p>Indonesian</p>
                    </td>
                    <td>
                      <p>Buddha</p>
                    </td>
                    <td>
                      <p>F</p>
                    </td>
                    <td>
                      <p>Pre - K</p>
                    </td>
                    <td>
                      <p>Morning</p>
                    </td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <div class="d-flex flex-row px-4 py-3 align-items-center">
              <div class="me-auto">
                <p class="mb-0 text-primary">Total Data : <b>10</b></p>
              </div>
              <div class="ms-auto">
                <nav aria-label="Page navigation" class="act-pagination">
                  <ul class="pagination justify-content-end">
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true"><i class="uil uil-angle-left-b"></i></span>
                      </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true"><i class="uil uil-angle-right-b"></i></span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>


<?php $__env->stopSection(); ?>
<?php $__env->startPush('js'); ?>
<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
      labels: ['Male', 'Female'],
      datasets: [{
          label: ['# Gender'],
          data: [200, 100, 500],
          backgroundColor: [
              'rgba(1, 78, 148, 1)',
              'rgba(202, 60, 37, 1)',
          ],
          borderColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)'
          ],
          borderWidth: 0
      }]
  }
});
</script>

<script>
var ctx = document.getElementById('myChart2').getContext('2d');
var myChart = new Chart(ctx, {
  type: 'doughnut',
  data: {
      labels: ['Student', 'Visitor', 'Candidate Student', 'Family'],
      datasets: [{
          data: [461, 25, 10, 461],
          backgroundColor: [
              'rgba(1, 78, 148, 1)',
              'rgba(202, 60, 37, 1)',
              'rgba(84, 140, 47, 1)',
              'rgba(5, 197, 197, 1)'
          ],
          borderColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)'
          ],
          borderWidth: 0
      }]
  },
});
</script>
<script type="text/javascript">
    $('#dashboardPage').addClass('active');
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make("admin.layouts.app", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/frediansimanjuntak/Documents/Freelance/websitesgia/resources/views/admin/index.blade.php ENDPATH**/ ?>