<?php $__env->startSection('title', 'AdminSGIA - Navbar Content'); ?>
<?php $__env->startSection('plugins.Datatables', true); ?>


<?php
$heads = [
    'ID',
    'Navbar',
    'Title',
    ['label' => 'Actions', 'no-export' => true, 'width' => 5],
];
$data = array();
$i = 1;
foreach ($contents as $content) {
    $btnEdit = '<a href="'.route('admin.navbars.contents.edit', $content).'" class="btn btn-xs btn-default text-teal mx-1 shadow"><i class="fa fa-lg fa-fw fa-pen"></i></a>';
    $btnDelete = '<button class="btn btn-xs btn-default text-danger mx-1 shadow" title="Delete">
                    <i class="fa fa-lg fa-fw fa-trash"></i>
                </button>';
    $btnDetails = '<a href="'.route('admin.navbars.contents.show', $content).'" class="btn btn-xs btn-default text-teal mx-1 shadow"><i class="fa fa-lg fa-fw fa-eye"></i></a>';

    $navbar = $content->navbar;
    $navbarParent = $content->navbar->parent()->first();
    $navbarSlug = isset($navbarParent) ? '/'.$navbarParent->slug.'/'.$navbar->slug : '/'.$navbar->slug;
    // dd($navbarSlug);
    
    
    $bar = array(
        $i,
        $navbarSlug,
        $content->title,
        '<nobr>'.$btnEdit.$btnDelete.$btnDetails.'</nobr>'
    );
    $data[] = $bar;
    $i += 1;
}


$config = [
    'data' => $data,
    'order' => [[1, 'asc']],
    'columns' => [null, null, ['orderable' => false]],
];
// dd($config['data']);
?>

<?php $__env->startSection('content_header'); ?>
    
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Navbar Contents</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?php echo e(route('admin.navbars.index')); ?>">Navbars</a></li>
            <li class="breadcrumb-item active">Contents</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
    <?php if(session('message')): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert <?php echo e(session('class')); ?> alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo e(session('message')); ?>

                </div>
            </div>
        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Navbar Content List</h3>
                    <div class="card-tools">
                        <a href="<?php echo e(route('admin.navbars.contents.create')); ?>" class="btn btn-default bg-teal">Add Content</a>
                    </div>
                </div>
                <!-- ./card-header -->
                <div class="card-body">
                    <?php if (isset($component)) { $__componentOriginal5c3a95af394031b1bfa79f2f00cd6019494000a3 = $component; } ?>
<?php $component = $__env->getContainer()->make(JeroenNoten\LaravelAdminLte\Components\Tool\Datatable::class, ['id' => 'table1','heads' => $heads]); ?>
<?php $component->withName('adminlte-datatable'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php $component->withAttributes([]); ?>
                        <?php $__currentLoopData = $config['data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <?php $__currentLoopData = $row; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cell): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <td><?php echo $cell; ?></td>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php if (isset($__componentOriginal5c3a95af394031b1bfa79f2f00cd6019494000a3)): ?>
<?php $component = $__componentOriginal5c3a95af394031b1bfa79f2f00cd6019494000a3; ?>
<?php unset($__componentOriginal5c3a95af394031b1bfa79f2f00cd6019494000a3); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        $(document).ready(function(){
            
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlte::page', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/frediansimanjuntak/Documents/Freelance/websitesgia/resources/views/admin-profile/navbar-contents/index.blade.php ENDPATH**/ ?>