<?php $__env->startSection('title', 'Admin - Create Navbar'); ?>

<?php $__env->startSection('content_header'); ?>
    
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Create Navbars</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?php echo e(route('admin.navbars.index')); ?>">Navbars</a></li>
            <li class="breadcrumb-item active">Create</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlte::page', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/frediansimanjuntak/Documents/Freelance/websitesgia/resources/views/admin-profile/navbars/create.blade.php ENDPATH**/ ?>