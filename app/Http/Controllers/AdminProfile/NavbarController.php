<?php

namespace App\Http\Controllers\AdminProfile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Navbar;
use Illuminate\Support\Str;

class NavbarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $navbars = Navbar::whereNull('parent_id')->orderBy('sequence', 'asc')->get();
        return view('admin-profile.navbars.index', compact('navbars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin-profile.navbars.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
        ]);

        if(isset($request['parent_id'])){
            $last_navbar = Navbar::where('parent_id', $request['parent_id'])->latest()->first();
        } else {
            $last_navbar = Navbar::latest()->first();
        }
        $request['slug'] = Str::slug($request['title']);
        $request['sequence'] = 1;
        if(isset($last_navbar)){
            $request['sequence'] = $last_navbar->sequence + 1;
        }
        Navbar::create($request->all());

        if(isset($request['parent_id'])){
            $navbarParent = Navbar::find($request['parent_id']);
            return redirect()->route('admin.navbars.show', $navbarParent)
                ->with('success', 'Navbar created successfully.');
        } else {
            return redirect()->route('admin.navbars.index')
                ->with('success', 'Navbar created successfully.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Navbar  $navbar
     * @return \Illuminate\Http\Response
     */
    public function show(Navbar $navbar)
    {
        $navbarChild = $navbar->children()->orderBy('sequence', 'asc')->get();
        return view('admin-profile.navbars.show', compact('navbar', 'navbarChild'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Navbar  $navbar
     * @return \Illuminate\Http\Response
     */
    public function edit(Navbar $navbar)
    {
        return view('admin-profile.navbars.edit', compact('navbar'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Navbar  $navbar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Navbar $navbar)
    {
        $request->validate([
            'title' => 'required',
            'sequence' => 'required'
        ]);
        $request['slug'] = Str::slug($request['title']);
        if(isset($request['sequence']) && $request['sequence'] != 0){
            if(isset($request['parent_id'])){
                $prevNavbar = Navbar::where('parent_id', $request['parent_id'])->where('sequence', $request['sequence'])->first();
            } else {
                $prevNavbar = Navbar::where('sequence', $request['sequence'])->first();
            }
            if(isset($prevNavbar)) {
                $prevNavbar->update(array('sequence'=>$navbar->sequence));
                $navbar->update($request->all());
                $status = array(
                    'class' => "alert-success",
                    'message' => "Navbar updated successfully"
                );

                if(isset($request['parent_id'])){
                    $navbarParent = Navbar::find($request['parent_id']);
                    return redirect()->route('admin.navbars.show', $navbarParent)
                        ->with($status);                    
                } else {
                    return redirect()->route('admin.navbars.index')
                        ->with($status);
                }
            } else {
                $status = array(
                    'class' => "alert-danger",
                    'message' => "Previous or Next Navbar position not found"
                );
                return back()->with($status);
            }
        } else {
            $status = array(
                'class' => "alert-danger",
                'message' => "Previous or Next Navbar position not found"
            );
            return back()->with($status);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Navbar  $navbar
     * @return \Illuminate\Http\Response
     */
    public function destroy(Navbar $navbar)
    {
        $navbar->delete();

        return redirect()->route('admin.navbars.index')
            ->with('success', 'Navbar deleted successfully');
    }
}
