<?php

namespace App\Http\Controllers\AdminProfile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FileDownload;
use Illuminate\Support\Facades\Storage;

class FileDownloadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $filedownloads = FileDownload::orderBy('created_at', 'desc')->get();
        return view('admin-profile.filedownloads.index', compact('filedownloads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin-profile.filedownloads.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
        ]);
        // dd($request->all());
        $this->validate($request, ['file' => 'required|mimes:pdf|max:10000']);
        if($request->hasfile('file'))
        {
            $path = Storage::disk('s3')->put('downloads', $request->file, 'public');
            $path = Storage::disk('s3')->url($path);
            $request['file'] = $request->file->getClientOriginalName();
            $request['file_path'] = $path;
            FileDownload::create($request->all());
            return redirect()->route('admin.filedownloads.index')
                    ->with('success', 'File Download created successfully.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
