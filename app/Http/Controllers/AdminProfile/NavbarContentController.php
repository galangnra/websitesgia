<?php

namespace App\Http\Controllers\AdminProfile;

use App\Http\Controllers\Controller;
use App\Models\Navbar;
use App\Models\NavbarContent;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class NavbarContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contents = NavbarContent::all();
        return view('admin-profile.navbar-contents.index', compact('contents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $navbarUrls = $this->getNavbarUrls();
        return view('admin-profile.navbar-contents.create', compact('navbarUrls'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
        ]);        
        $request['slug'] = Str::slug($request['title']);
        NavbarContent::create($request->all());
        return redirect()->route('admin.navbars.contents.index')
                ->with('success', 'Navbar Content created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NavbarContent  $navbarContent
     * @return \Illuminate\Http\Response
     */
    public function show(NavbarContent $navbarContent)
    {
        return view('admin-profile.navbar-contents.show', compact('navbarContent'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NavbarContent  $navbarContent
     * @return \Illuminate\Http\Response
     */
    public function edit(NavbarContent $navbarContent, $id)
    {
        $navbarContent = NavbarContent::find($id);
        $navbarUrls = $this->getNavbarUrls();

        return view('admin-profile.navbar-contents.edit', compact('navbarContent', 'navbarUrls'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NavbarContent  $navbarContent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
        ]);
        $request['slug'] = Str::slug($request['title']);
        $navbarContent = NavbarContent::find($id);
        $navbarContent->update($request->all());
        $status = array(
            'class' => "alert-success",
            'message' => "Navbar Content updated successfully"
        );
        return redirect()->route('admin.navbars.contents.index')
            ->with($status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NavbarContent  $navbarContent
     * @return \Illuminate\Http\Response
     */
    public function destroy(NavbarContent $navbarContent)
    {
        $navbarContent->delete();

        return redirect()->route('admin.navbars.contents.index')
            ->with('success', 'Navbar Content deleted successfully');
    }

    private function getNavbarUrls(){
        $navbars = Navbar::whereNull('parent_id')->get();
        $navbarUrls = collect();
        foreach ($navbars as $navbar) {
            // dd($navbar->children()->count());
            if($navbar->children()->get()->count() > 0){
                $navbarChildren = $navbar->children()->get();
                foreach ($navbarChildren as $navbarChild) {
                    $data = array(
                        'id' => $navbarChild->id,
                        'url' => '/'.$navbar->slug.'/'.$navbarChild->slug
                    );
                    $navbarUrls->push((object)$data);
                }
            }
            if($navbar->children()->get()->count() == 0){
                $data = array(
                    'id' => $navbar->id,
                    'url' => '/'.$navbar->slug
                );
                $navbarUrls->push((object)$data);
            }
        }
        return $navbarUrls;
    }
}
