<?php

namespace App\Http\Controllers\AdminProfile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Content;
use Illuminate\Support\Str;
class ContentController extends Controller
{
    public function index()
    {
        $contents = Content::get();
        // dd($navbars);
        return view('admin-profile.contents.index', compact('contents'));
    }
}
