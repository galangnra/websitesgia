<?php

namespace App\Http\Controllers\FrontendProfile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Navbar;
use App\Models\NavbarContent;

class PageController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($URI)
    {
        $navbar = Navbar::where('slug', $URI)->first();
        $navbarContent = NavbarContent::where('navbar_id', $navbar->id)->first();
        return view('frontend-profile.pages.show', compact('navbar', 'navbarContent'));
    }
}
