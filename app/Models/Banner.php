<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;


class Banner extends Model
{
    use Uuid, HasFactory;

    public $timestamps = true;

    protected $fillable = [
        'title',
        'image',
        'file_path',
        'sequence',
        'is_active'
    ];
}
