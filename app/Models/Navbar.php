<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Navbar extends Model
{
    use Uuid, HasFactory;
    protected $parentColumn = 'parent_id';
    public $timestamps = true;
    
    protected $fillable = [
        'title',
        'parent_id',
        'slug',
        'sequence',
        'is_active'
    ];

    public function parent()
    {
        return $this->belongsTo(Navbar::class,$this->parentColumn);
    }

    public function children()
    {
        return $this->hasMany(Navbar::class, $this->parentColumn);
    }
}
