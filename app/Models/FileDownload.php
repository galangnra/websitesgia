<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class FileDownload extends Model
{
    use Uuid, HasFactory;

    public $timestamps = true;

    protected $fillable = [
        'title',
        'file',
        'file_path',
        'is_active'
    ];
}
