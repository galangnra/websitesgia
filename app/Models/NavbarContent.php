<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Navbar;

class NavbarContent extends Model
{
    use Uuid, HasFactory;

    public $timestamps = true;
    
    protected $fillable = [
        'title',
        'slug',
        'content_type',
        'content',
        'published_at',
        'navbar_id',
        'is_active'
    ];

    public function navbar()
    {
        return $this->belongsTo(Navbar::class, 'navbar_id');
    }
}
