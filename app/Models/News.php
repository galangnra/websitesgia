<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use Uuid, HasFactory;

    public $timestamps = true;

    protected $fillable = [
        'title',
        'slug',
        'content_type',
        'content',
        'published_at',
        'is_active'
    ];
}
