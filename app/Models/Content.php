<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    use Uuid, HasFactory;

    public $incrementing = false;

    protected $keyType = 'uuid';
    
    public function navbar()
    {
        return $this->belongsTo(Navbar::class);
    }
}
