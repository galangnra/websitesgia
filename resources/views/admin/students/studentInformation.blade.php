@extends("admin.layouts.app")
@section("content")

<section class="mb-4">
    <div class="container-fluid actwrap">
        <div class="row">
            <div class="col-12">
                <h4>Student Information</h4>
                <p>Student Information Data</p>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid actwrap">
    <div class="row">
        <div class="col-lg-12">
          <div class="card card-table nbs mt-4">

            <div class="d-flex flex-row w-100 align-items-end py-3 px-4 mb-4">
              <div class="px-2">
                <label for="">Search Student by ID/Name</label>
                <input type="text" name="" value="" class="form-control" placeholder="Enter Student by ID/Name">
              </div>

              <div class="px-2">
                <label for="">Search by Grade</label>
                <select class="form-control" name="">
                    <option value="" selected disabled>-- Select Grade --</option>
                    <option value=""></option>
                    <option value=""></option>
                </select>
              </div>

              <div class="px-2">
                <label for="">Search by Status</label>
                <select class="form-control" name="">
                    <option value="" selected disabled>-- Select Status --</option>
                    <option value=""></option>
                    <option value=""></option>
                </select>
              </div>
              <div class="px-2">
                <button href="#" class="input-group-text btn bg-primary text-white"><i class="uil uil-search"></i> Search</button>
              </div>
            </div>

            <div class="table-responsive">
              <table class="table table-stripped">
                <thead>
                  <th>#</th>
                  <th>ID</th>
                  <th>Student Name</th>
                  <th>NISN</th>
                  <th>Nationality</th>
                  <th>Religion</th>
                  <th>Gender</th>
                  <th>OnGoingClass</th>
                  <th>ClassDif</th>
                  <th>Details</th>
                </thead>
                <tbody>
                  <?php for ($i=1; $i < 2 ; $i++) {
                   ?>
                  <tr>
                    <td>
                      <p><?php echo($i) ?></p>
                    </td>
                    <td>
                      <p>0101140360</p>
                    </td>
                    <td>
                      <p>Shiane Monica L De Zilva</p>
                    </td>
                    <td>
                      <p>9992420399</p>
                    </td>
                    <td>
                      <p>Indonesian</p>
                    </td>
                    <td>
                      <p>Catholic</p>
                    </td>
                    <td>
                      <p>Female</p>
                    </td>
                    <td>
                      <p>Grade 12</p>
                    </td>
                    <td>
                      <p>None</p>
                    </td>
                    <td>
                        <p class="mx-1"><a href="{{url('/details-student-information')}}" class="nbb"><button type="button" name="button" class="btn btn-info bg-info">Details</button></a></p>
                    </td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>

            <div class="d-flex flex-row px-4 py-3 align-items-center">
              <div class="me-auto">
                <div class="input-group">
                  <label for="" class="input-group-text nbg">Show Data</label>
                  <select class="form-control" name="">
                    <option value="">5</option>
                    <option value="">10</option>
                    <option value="">50</option>
                  </select>
                </div>
              </div>
              <div class="ms-auto">
                <nav aria-label="Page navigation" class="act-pagination">
                  <ul class="pagination justify-content-end">
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true"><i class="uil uil-angle-left-b"></i></span>
                      </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true"><i class="uil uil-angle-right-b"></i></span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>

@endsection
@push('js')
  <script type="text/javascript">
      $('#studentsCollapse').addClass('collapse show');
  </script>
@endpush
