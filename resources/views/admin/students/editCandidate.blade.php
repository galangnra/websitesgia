@extends("admin.layouts.app")
@section('css')
@section("content")

<section class="mb-4">
    <div class="container-fluid actwrap">
        <div class="row">
            <div class="col-12">
                <h4>Add Candidate Student</h4>
                <p>fill in the form below</p>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid actwrap">
    <div class="row">
      <div class="col-lg-6">
          <div class="card act-card bg-white text-dark">
            <form class="" action="" method="post">
            <h5 class="mb-4">Student Information</h5>
            <div class="my-3">
              <label for="">School Year</label>
              <select class="form-control" name="">
                <option value="" disabled>-- Select School Years --</option>
                <option value="" selected>2016 - 2017</option>
                <option value="">2018 - 2019</option>
                <option value="">2020 - 2021</option>
              </select>
            </div>

            <div class="my-3">
              <label for="">Student Name</label>
              <input type="text" name="" value="Eugene Arief Vianata" class="form-control" placeholder="Enter Student Name">
            </div>

            <div class="my-3">
              <label for="">Grade</label>
              <select class="form-control" name="">
                <option value="" disabled selected>-- Select Grade --</option>
                <option value="" selected>Grade 12</option>
                <option value="">Grade 11</option>
              </select>
            </div>

            <div class="my-3">
              <label for="">Nationality</label>
              <select class="form-control" name="">
                <option value="" disabled >-- Select Nationality --</option>
                <option value=""selected>Spanish</option>
                <option value="">Malaysia</option>
                <option value="">Singapore</option>
                <option value="">Indonesia</option>
              </select>
            </div>

            <div class="my-3">
              <label for="">Status Student</label>
              <select class="form-control" name="">
                <option value="" disabled>-- Select Status Student --</option>
                <option value="" selected>WNA</option>
                <option value="">WNI</option>
              </select>
            </div>

            <h5 class="mb-4">Personal Information</h5>

            <div class="my-3">
              <label for="">Religion</label>
              <select class="form-control" name="">
                <option value="" disabled>-- Select Religion --</option>
                <option value="">Buddha</option>
                <option value="" selected>Islam</option>
                <option value="">Katolik</option>
                <option value="">Protestan</option>
                <option value="">Hindu</option>
              </select>
            </div>

            <div class="my-3">
              <label for="">Gender</label>
              <select class="form-control" name="">
                <option value="" disabled>-- Select Gender --</option>
                <option value="" selected>Male</option>
                <option value="">Female</option>
              </select>
            </div>

            <div class="my-3">
              <label for="">School Origin</label>
              <input type="text" name="" value="SMK10" class="form-control" placeholder="Enter School Origin">
            </div>

            <div class="my-3">
              <label for="">Birth Place</label>
              <input type="text" name="" value="Belakang Padang" class="form-control" placeholder="Enter Place of Birth">
            </div>

            <div class="my-3">
              <label for="">Birth Date</label>
              <input type="text" name="" value="01-06-2016" class="form-control" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="dd-mm-yyyy">
            </div>

            <div class="my-3">
              <label for="">Address</label>
              <textarea name="name" rows="8" cols="80" class="w-100 d-block form-control">Batam</textarea>
            </div>



            <div class="mt-4 float-end">
              <a href="{{url('/candidate')}}" class="nbb text-white"><button type="button" name="button" class="btn btn-secondary bg-secondary px-3 me-3">Cancel</button></a>
              <button type="button" name="button" class="btn btn-primary bg-primary px-5">Save</button>
            </div>
          </form>
          </div>
       </div>

        <div class="col-lg-6">
            <div class="card act-card bg-white text-dark">
              <h5 class="mb-4">Family Information</h5>
                <form class="" action="" method="post">
                  <h5>Father</h5>
                  <hr>
                  <div class="my-3">
                    <label for="">Father Name</label>
                    <input type="text" name="" value="Budi" class="form-control" placeholder="Enter Father Name">
                  </div>

                  <div class="my-3">
                    <label for="">Father Nationality</label>
                    <select class="form-control" name="">
                      <option value="" disabled>-- Select Nationality --</option>
                      <option value="" selected>Indonesian</option>
                      <option value="">Malaysia</option>
                      <option value="">Singapore</option>
                    </select>
                  </div>

                  <div class="my-3">
                    <label for="">Father Phone</label>
                    <input type="number" name="" value="0854122344578" class="form-control" placeholder="Enter Phone Number">
                  </div>


                  <div class="my-3">
                    <label for="">Father Email</label>
                    <input type="mail" name="" value="Budi@gmail.com" class="form-control" placeholder="Enter Father Email">
                  </div>

                  <h5 class="mt-5">Mother</h5>
                  <hr>
                  <div class="my-3">
                    <label for="">Mother Name</label>
                    <input type="text" name="" value="Ani" class="form-control" placeholder="Enter Mother Name">
                  </div>

                  <div class="my-3">
                    <label for="">Mother Nationality</label>
                    <select class="form-control" name="">
                      <option value="" disabled>-- Select Nationality --</option>
                      <option value="" selected>Indonesian</option>
                      <option value="">Malaysia</option>
                      <option value="">Singapore</option>
                    </select>
                  </div>

                  <div class="my-3">
                    <label for="">Mother Phone</label>
                    <input type="number" name="" value="0854122344578" class="form-control" placeholder="Enter Phone Number">
                  </div>


                  <div class="my-3">
                    <label for="">Mother Email</label>
                    <input type="mail" name="" value="Anii@gmail.com" class="form-control" placeholder="Enter Mother Email">
                  </div>
                </form>

            </div>
        </div>

    </div>
</div>


@endsection
@push('js')
  <script type="text/javascript">
      $('#studentsCollapse').addClass('collapse show');
  </script>
@endpush
