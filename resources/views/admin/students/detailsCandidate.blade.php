@extends("admin.layouts.app")
@section('css')
@section("content")

<section class="mb-4">
    <div class="container-fluid actwrap">
        <div class="row">
            <div class="col-12">
                <h4>Details Candidate Student</h4>
                <p>See more detailed Candidate Student Data</p>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid actwrap">
    <div class="row">
      <div class="col-lg-6">
          <div class="card act-card bg-white text-dark">
            <form class="" action="" method="post">
            <h5 class="mb-4">Student Information</h5>
            <div class="my-3">
              <label for="">School Year</label>
              <input type="text" name="" value="2016-2017" disabled class="form-control">
            </div>

            <div class="my-3">
              <label for="">Student Name</label>
              <input type="text" name="" value="Eugene Arief Vianata" class="form-control" disabled>
            </div>

            <div class="my-3">
              <label for="">Grade</label>
              <input type="text" name="" value="Grade 12" disabled class="form-control">
            </div>

            <div class="my-3">
              <label for="">Nationality</label>
              <input type="text" name="" value="Spanish" disabled class="form-control">
            </div>

            <div class="my-3">
              <label for="">Status Student</label>
              <input type="text" name="" value="WNA" disabled class="form-control">
            </div>

            <h5 class="mb-4">Personal Information</h5>

            <div class="my-3">
              <label for="">Religion</label>
              <input type="text" name="" value="Islam" disabled class="form-control">
            </div>

            <div class="my-3">
              <label for="">Gender</label>
              <input type="text" name="" value="Male" disabled class="form-control">
            </div>

            <div class="my-3">
              <label for="">School Origin</label>
              <input type="text" name="" value="SMK10" class="form-control" disabled>
            </div>

            <div class="my-3">
              <label for="">Birth Place</label>
              <input type="text" name="" value="Belakang Padang" class="form-control" disabled>
            </div>

            <div class="my-3">
              <label for="">Birth Date</label>
              <input type="text" name="" value="01 June 2016" class="form-control" disabled>
            </div>

            <div class="my-3">
              <label for="">Address</label>
              <input type="text" name="" value="Batam" class="form-control" disabled>
            </div>

          </form>
          </div>

          <div class="card act-card bg-white text-dark my-4">
            <h5 class="mb-4">Family Information</h5>
              <form class="" action="" method="post">
                <h5>Father</h5>
                <hr>
                <div class="my-3">
                  <label for="">Father Name</label>
                  <input type="text" name="" value="Budi" class="form-control" disabled>
                </div>

                <div class="my-3">
                  <label for="">Father Nationality</label>
                  <input type="text" name="" value="Indonesian" class="form-control" disabled>
                </div>

                <div class="my-3">
                  <label for="">Father Phone</label>
                  <input type="number" name="" value="0854122344578" class="form-control" disabled>
                </div>


                <div class="my-3">
                  <label for="">Father Email</label>
                  <input type="mail" name="" value="Budi@gmail.com" class="form-control" disabled>
                </div>

                <h5 class="mt-5">Mother</h5>
                <hr>
                <div class="my-3">
                  <label for="">Mother Name</label>
                  <input type="text" name="" value="Ani" class="form-control" disabled>
                </div>

                <div class="my-3">
                  <label for="">Mother Nationality</label>
                  <input type="text" name="" value="Indonesian" class="form-control" disabled>
                </div>

                <div class="my-3">
                  <label for="">Mother Phone</label>
                  <input type="number" name="" value="0854122344578" class="form-control" disabled>
                </div>


                <div class="my-3">
                  <label for="">Mother Email</label>
                  <input type="mail" name="" value="Anii@gmail.com" class="form-control" disabled>
                </div>
              </form>

          </div>

       </div>

        <div class="col-lg-6">
            <div class="card act-card bg-white text-dark">
              <div class="my-3">
                <label for="">Status</label>
                <input type="text" name="" value="Candidate 16" class="form-control" disabled>
              </div>
              <div class="my-3">
                <label for="">Status</label>
                <select class="form-control" name="">
                  <option value="">Candidate</option>
                  <option value=""></option>
                  <option value=""></option>
                </select>
              </div>
              <div class="my-3">
                <label for="">Description</label>
                <textarea name="name" rows="8" cols="80" class="w-100 d-block form-control"></textarea>
              </div>

              <div class="mt-4 text-end">
                <a href="{{url('/candidate')}}" class="nbb text-white"><button type="button" name="button" class="btn btn-secondary bg-secondary px-3 me-3">Cancel</button></a>
                <button type="button" name="button" class="btn btn-primary bg-primary px-5">Proccess</button>
              </div>
            </div>
        </div>

    </div>
</div>


@endsection
@push('js')
  <script type="text/javascript">
      $('#studentsCollapse').addClass('collapse show');
  </script>
@endpush
