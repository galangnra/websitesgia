@extends("admin.layouts.app")
@section('css')
@section("content")

<section class="mb-4">
    <div class="container-fluid actwrap">
        <div class="row">
            <div class="col-12">
                <h4>Add Candidate Student</h4>
                <p>fill in the form below</p>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid actwrap">
    <div class="row">
        <div class="col-lg-6">
            <div class="card act-card bg-white text-dark">
              <h5 class="mb-4">Candidate Student Information</h5>
                <form class="" action="" method="post">
                  <div class="my-3">
                    <label for="">School Year</label>
                    <select class="form-control" name="">
                      <option value="" disabled selected>-- Select School Years --</option>
                      <option value="">2019</option>
                      <option value="">2020</option>
                      <option value="">2021</option>
                    </select>
                  </div>

                  <div class="my-3">
                    <label for="">Student Name</label>
                    <input type="text" name="" value="" class="form-control" placeholder="Enter Student Name">
                  </div>

                  <div class="my-3">
                    <label for="">Place of Birth</label>
                    <input type="text" name="" value="" class="form-control" placeholder="Enter Place of Birth">
                  </div>

                  <div class="my-3">
                    <label for="">Date of Birth</label>
                    <input type="date" name="" value="" class="form-control">
                  </div>

                  <div class="my-3">
                    <label for="">Address</label>
                    <textarea name="name" rows="8" cols="80" class="w-100 d-block form-control"></textarea>
                  </div>

                  <div class="my-3">
                    <label for="">Religion</label>
                    <select class="form-control" name="">
                      <option value="" disabled selected>-- Select Religion --</option>
                      <option value="">Buddha</option>
                      <option value="">Islam</option>
                      <option value="">Katolik</option>
                      <option value="">Protestan</option>
                      <option value="">Hindu</option>
                    </select>
                  </div>

                  <div class="my-3">
                    <label for="">Gender</label>
                    <select class="form-control" name="">
                      <option value="" disabled selected>-- Select Gender --</option>
                      <option value="">Male</option>
                      <option value="">Female</option>
                    </select>
                  </div>

                  <div class="my-3">
                    <label for="">Join Class</label>
                    <select class="form-control" name="">
                      <option value="" disabled selected>-- Select Grade --</option>
                      <option value=""></option>
                      <option value=""></option>
                    </select>
                  </div>

                  <div class="my-3">
                    <label for="">Nationality</label>
                    <select class="form-control" name="">
                      <option value="" disabled selected>-- Select Nationality --</option>
                      <option value="">Indonesia</option>
                      <option value="">Malaysia</option>
                      <option value="">Singapore</option>
                    </select>
                  </div>

                  <div class="my-3">
                    <label for="">Status Student</label>
                    <select class="form-control" name="">
                      <option value="" disabled selected>-- Select Status Student --</option>
                      <option value="">Active</option>
                      <option value="">None Active</option>
                    </select>
                  </div>

                  <div class="my-3">
                    <label for="">School Origin</label>
                    <input type="text" name="" value="" class="form-control" placeholder="Enter School Origin">
                  </div>

                  <div class="mt-4 float-end">
                    <button type="button" name="button" class="btn btn-secondary bg-secondary px-3 me-3">Cancel</button>
                    <button type="button" name="button" class="btn btn-primary bg-primary px-5">Save</button>
                  </div>
                </form>

            </div>
        </div>

        <div class="col-lg-6">
            <div class="card act-card bg-white text-dark">
              <h5 class="mb-4">Family Information</h5>
                <div class="d-flex flex-row mb-4">
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="Family" id="newFamily">
                    <label class="form-check-label" for="newFamily">
                      New Family
                    </label>
                  </div>
                  <div class="form-check ms-4">
                    <input class="form-check-input" type="radio" name="Family" id="currentFamily" checked data-bs-toggle="modal" data-bs-target="#currentFamilyModal">
                    <label class="form-check-label" for="oldFamily">
                      Current Family
                    </label>
                  </div>
                </div>

                <form class="" action="" method="post">
                  <h5>Father</h5>
                  <hr>
                  <div class="my-3">
                    <label for="">Name</label>
                    <input type="text" name="" value="" class="form-control" placeholder="Enter Father Name">
                  </div>

                  <div class="my-3">
                    <label for="">Phone</label>
                    <input type="number" name="" value="" class="form-control" placeholder="Enter Phone Number">
                  </div>

                  <div class="my-3">
                    <label for="">Nationality</label>
                    <select class="form-control" name="">
                      <option value="" disabled selected>-- Select Nationality --</option>
                      <option value="">Indonesia</option>
                      <option value="">Malaysia</option>
                      <option value="">Singapore</option>
                    </select>
                  </div>

                  <div class="my-3">
                    <label for="">Email</label>
                    <input type="mail" name="" value="" class="form-control" placeholder="Enter Father Email">
                  </div>

                  <h5 class="mt-5">Mother</h5>
                  <hr>
                  <div class="my-3">
                    <label for="">Name</label>
                    <input type="text" name="" value="" class="form-control" placeholder="Enter Mother Name">
                  </div>

                  <div class="my-3">
                    <label for="">Phone</label>
                    <input type="number" name="" value="" class="form-control" placeholder="Enter Phone Number">
                  </div>

                  <div class="my-3">
                    <label for="">Nationality</label>
                    <select class="form-control" name="">
                      <option value="" disabled selected>-- Select Nationality --</option>
                      <option value="">Indonesia</option>
                      <option value="">Malaysia</option>
                      <option value="">Singapore</option>
                    </select>
                  </div>

                  <div class="my-3">
                    <label for="">Email</label>
                    <input type="mail" name="" value="" class="form-control" placeholder="Enter Mother Email">
                  </div>
                </form>

            </div>
        </div>

    </div>
</div>

<!-- Modal Current Family -->
<div class="modal fade" id="currentFamilyModal" tabindex="-1" aria-labelledby="currentFamilyModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Family</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <label for="">Search Family</label>
        <div class="input-group mb-3 mt-2">
          <input type="text" name="" value="" class="form-control" placeholder="Enter Father Name / Mother Name">
          <button href="#" class="input-group-text btn bg-primary text-white"><i class="uil uil-search"></i></button>
        </div>
        <div class="card card-table nbs mt-4">
          <div class="table-responsive">
            <table class="table table-stripped">
              <thead>
                <th>#</th>
                <th>Father Name</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Nationality</th>
                <th>Mother Name</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Nationality</th>
                <th>Action</th>
              </thead>
              <tbody>
                <?php for ($i=1; $i < 6 ; $i++) {
                 ?>
                <tr>
                  <td>
                    <p><?php echo($i) ?></p>
                  </td>
                  <td>
                    <p>Iskandar Itan</p>
                  </td>
                  <td>
                    <p>isk_itn@yahoo.com</p>
                  </td>
                  <td>
                    <p>082173739639</p>
                  </td>
                  <td>
                    <p>Indonesian</p>
                  </td>
                  <td>
                    <p>Christie Mondoringin</p>
                  </td>
                  <td>
                    <p>chrissy0789@yahoo.com.sg</p>
                  </td>
                  <td>
                    <p>08127004226</p>
                  </td>
                  <td>
                    <p>Indonesian</p>
                  </td>
                  <td>
                    <p><button type="button" name="button" class="btn btn-primary bg-primary">Select</button></p>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>

          <div class="d-flex flex-row px-4 py-3 align-items-center">
            <div class="me-auto">
              <div class="input-group">
                <label for="" class="input-group-text nbg">Show Data</label>
                <select class="form-control" name="">
                  <option value="">5</option>
                  <option value="">10</option>
                  <option value="">50</option>
                </select>
              </div>
            </div>
            <div class="ms-auto">
              <nav aria-label="Page navigation" class="act-pagination">
                <ul class="pagination justify-content-end">
                  <li class="page-item">
                    <a class="page-link" href="#" aria-label="Previous">
                      <span aria-hidden="true"><i class="uil uil-angle-left-b"></i></span>
                    </a>
                  </li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item">
                    <a class="page-link" href="#" aria-label="Next">
                      <span aria-hidden="true"><i class="uil uil-angle-right-b"></i></span>
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>


@endsection
@push('js')
  <script type="text/javascript">
      $('#studentsCollapse').addClass('collapse show');
  </script>
@endpush
