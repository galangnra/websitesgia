@extends("admin.layouts.app")
@section('css')
@section("content")

<section class="mb-4">
    <div class="container-fluid actwrap">
        <div class="row">
            <div class="col-12">
                <h4>Student Details</h4>
                <p>See more detailed Student Data</p>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid actwrap">
    <div class="row">
      <div class="col-lg-3">
        <div class="card act-card bg-white text-dark profile-picture">
          <img src="{{asset('assets/imgs/dummy-pp.jpg')}}" alt="" width="100%">
          <h5 class="mb-0">Shiane Monica L De Zilva</h5>
          <h6 class="mb-3">Indonesian</h6>
          <p class="mb-1">Student ID : <b>0101140360</b></p>
          <p class="mb-1">NISN : <b>9992420399</b></p>
          <p class="mb-1">Inden Number : <b>-</b></p>
        </div>
      </div>

      <div class="col-lg-9">
          <div class="card act-card bg-white text-dark">
            <form class="" action="" method="post">
            <!-- <h5 class="mb-4">Student Information</h5>
            <div class="my-3">
              <label for="">Student Id</label>
              <input type="text" name="" value="0101140360" disabled class="form-control">
            </div>

            <div class="my-3">
              <label for="">Student Name</label>
              <input type="text" name="" value="Shiane Monica L De Zilva" class="form-control" disabled>
            </div>

            <div class="my-3">
              <label for="">NISN</label>
              <input type="text" name="" value="9992420399" disabled class="form-control">
            </div>

            <div class="my-3">
              <label for="">Inden Number</label>
              <input type="text" name="" value="" disabled class="form-control">
            </div>

            <div class="my-3">
              <label for="">Nationality</label>
              <input type="text" name="" value="Indonesian" disabled class="form-control">
            </div> -->

            <h5 class="my-4">Personal Information</h5>

            <div class="my-3">
              <label for="">Birth Place</label>
              <input type="text" name="" value="Indonesian" class="form-control" disabled>
            </div>

            <div class="my-3">
              <label for="">Birth Date</label>
              <input type="text" name="" value="14 September 1998" class="form-control" disabled>
            </div>

            <div class="my-3">
              <label for="">Address</label>
              <textarea name="name" rows="8" cols="80" class="w-100 form-control">Bukit Permata, Jl. Permata Raya No. 1</textarea>
            </div>

            <div class="my-3">
              <label for="">Religion</label>
              <input type="text" name="" value="Catholic" disabled class="form-control">
            </div>

            <div class="my-3">
              <label for="">Gender</label>
              <input type="text" name="" value="Female" disabled class="form-control">
            </div>

            <div class="my-3">
              <label for="">Family</label>
              <input type="text" name="" value="Anas" class="form-control" disabled>
            </div>

              <h5 class="my-5">Family Information</h5>
                <form class="" action="" method="post">
                  <h5>Father</h5>
                  <hr>
                  <div class="my-3">
                    <label for="">Father Name</label>
                    <input type="text" name="" value="Anas" class="form-control" disabled>
                  </div>

                  <div class="my-3">
                    <label for="">Phone</label>
                    <input type="number" name="" value="0854122344578" class="form-control" disabled>
                  </div>

                  <div class="my-3">
                    <label for="">Email</label>
                    <input type="text" name="" value="anasktc@indosat.net.id" class="form-control" disabled>
                  </div>

                  <h5 class="mt-5">Mother</h5>
                  <hr>
                  <div class="my-3">
                    <label for="">Mother Name</label>
                    <input type="text" name="" value="Ani" class="form-control" disabled>
                  </div>

                  <div class="my-3">
                    <label for="">Phone</label>
                    <input type="number" name="" value="0854122344578" class="form-control" disabled>
                  </div>

                  <div class="my-3">
                    <label for="">Email</label>
                    <input type="mail" name="" value="Anii@gmail.com" class="form-control" disabled>
                  </div>

            <div class="text-end mt-4">
              <a href="{{url('/student-information')}}" class="nbb text-white"><button type="button" name="button" class="btn btn-secondary bg-secondary px-4">Ok</button></a>
            </div>
          </form>
          </div>
       </div>

    </div>
</div>


@endsection
@push('js')
  <script type="text/javascript">
      $('#studentsCollapse').addClass('collapse show');
  </script>
@endpush
