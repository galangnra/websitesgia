@extends("admin.layouts.app")
@section("content")

<section class="mb-4">
    <div class="container-fluid actwrap">
        <div class="row">
            <div class="col-12">
                <h4>Nationality</h4>
                <p>Detail About Nationality</p>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid actwrap">
    <div class="row">
        <div class="col-lg-6">
          <div class="card card-table nbs">

            <div class="d-flex flex-row w-100 align-items-end py-3 px-4 mb-4">
              <div class="px-2">
                <label for="">Search Data</label>
                <input type="text" name="" value="" class="form-control" placeholder="Enter Grade">
              </div>
              <div class="px-2">
                <button href="#" class="input-group-text btn bg-primary text-white"><i class="uil uil-search"></i> Search</button>
              </div>
              <div class="px-2 ms-auto">
                  <a href="#" class="btn btn-success bg-success" data-bs-toggle="modal" data-bs-target="#addNationality"><i class="uil uil-plus-circle"></i> Add Nationality</a>
              </div>
            </div>

            <div class="table-responsive">
              <table class="table table-stripped">
                <thead>
                  <th>#</th>
                  <th>Nationality</th>
                  <th>Action</th>
                </thead>
                <tbody>
                  <?php for ($i=1; $i < 2 ; $i++) {
                   ?>
                  <tr>
                    <td>
                      <p><?php echo($i) ?></p>
                    </td>
                    <td>
                      <p>American</p>
                    </td>
                    <td>
                        <div class="d-flex flex-row">
                          <p class="mx-1"><a href="#" class="nbb"><button type="button" name="button" class="btn btn-warning bg-warning" data-bs-toggle="modal" data-bs-target="#editNationality">Edit</button></a></p>
                          <p class="mx-1"><a href="#" class="nbb"><button type="button" name="button" class="btn btn-danger bg-danger">Delete</button></a></p>
                        </div>
                    </td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>

            <div class="d-flex flex-row px-4 py-3 align-items-center">
              <div class="me-auto">
                <div class="input-group">
                  <label for="" class="input-group-text nbg">Show Data</label>
                  <select class="form-control" name="">
                    <option value="">5</option>
                    <option value="">10</option>
                    <option value="">50</option>
                  </select>
                </div>
              </div>
              <div class="ms-auto">
                <nav aria-label="Page navigation" class="act-pagination">
                  <ul class="pagination justify-content-end">
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true"><i class="uil uil-angle-left-b"></i></span>
                      </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true"><i class="uil uil-angle-right-b"></i></span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>


<!-- Modal Add Grade -->
<div class="modal fade" id="addNationality" tabindex="-1" aria-labelledby="addNationalityLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Nationality</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="px-5">
          <div class="my-3">
            <label for="">Nationality</label>
              <input type="text" name="" value="" placeholder="Enter Country" class="form-control">
          </div>

          <div class="d-flex flex-row justify-content-between mt-4 mb-2">
            <a href="#" class="btn btn-secondary bg-secondary col-lg-4 my-2" data-bs-dismiss="modal" aria-label="Close">Cancel</a>
            <a href="#" class="btn btn-success bg-success col-lg-4 my-2">Save</a>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<!-- Modal Edit Grade -->
<div class="modal fade" id="editNationality" tabindex="-1" aria-labelledby="editNationalityLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Nationality</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="px-5">
          <div class="my-3">
            <label for="">Nationality</label>
              <input type="text" name="" value="American" placeholder="Enter Country" class="form-control">
          </div>
          <div class="d-flex flex-row justify-content-between mt-4 mb-2">
            <a href="#" class="btn btn-secondary bg-secondary col-lg-4 my-2" data-bs-dismiss="modal" aria-label="Close">Cancel</a>
            <a href="#" class="btn btn-success bg-success col-lg-4 my-2">Save</a>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

@endsection
@push('js')
  <script type="text/javascript">
      $('#admissionCollapse').addClass('collapse show');
  </script>
@endpush
