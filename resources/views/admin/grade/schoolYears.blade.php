@extends("admin.layouts.app")
@section("content")

<section class="mb-4">
    <div class="container-fluid actwrap">
        <div class="row">
            <div class="col-12">
                <h4>School Years</h4>
                <p>Detail About School Years</p>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid actwrap">
    <div class="row">
        <div class="col-lg-6">
          <div class="card card-table nbs">

            <div class="d-flex flex-row w-100 align-items-end py-3 px-4 mb-4">
              <div class="px-2">
                <label for="">Search Data</label>
                <input type="text" name="" value="" class="form-control" placeholder="Enter Grade">
              </div>
              <div class="px-2">
                <button href="#" class="input-group-text btn bg-primary text-white"><i class="uil uil-search"></i> Search</button>
              </div>
              <div class="px-2 ms-auto">
                  <a href="#" class="btn btn-success bg-success" data-bs-toggle="modal" data-bs-target="#addSchoolyear"><i class="uil uil-plus-circle"></i> Add School Year</a>
              </div>
            </div>

            <div class="table-responsive">
              <table class="table table-stripped">
                <thead>
                  <th>#</th>
                  <th>School Year</th>
                  <th>Period</th>
                  <th>Action</th>
                </thead>
                <tbody>
                  <?php for ($i=1; $i < 2 ; $i++) {
                   ?>
                  <tr>
                    <td>
                      <p><?php echo($i) ?></p>
                    </td>
                    <td>
                      <p>2016-2017</p>
                    </td>
                    <td>
                      <p>July-June</p>
                    </td>
                    <td>
                        <div class="d-flex flex-row">
                          <p class="mx-1"><a href="#" class="nbb"><button type="button" name="button" class="btn btn-warning bg-warning" data-bs-toggle="modal" data-bs-target="#editSchoolyearModal">Edit</button></a></p>
                          <p class="mx-1"><a href="#" class="nbb"><button type="button" name="button" class="btn btn-danger bg-danger">Delete</button></a></p>
                        </div>
                    </td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>

            <div class="d-flex flex-row px-4 py-3 align-items-center">
              <div class="me-auto">
                <div class="input-group">
                  <label for="" class="input-group-text nbg">Show Data</label>
                  <select class="form-control" name="">
                    <option value="">5</option>
                    <option value="">10</option>
                    <option value="">50</option>
                  </select>
                </div>
              </div>
              <div class="ms-auto">
                <nav aria-label="Page navigation" class="act-pagination">
                  <ul class="pagination justify-content-end">
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true"><i class="uil uil-angle-left-b"></i></span>
                      </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true"><i class="uil uil-angle-right-b"></i></span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>


<!-- Modal Add Grade -->
<div class="modal fade" id="addSchoolyear" tabindex="-1" aria-labelledby="addSchoolyearLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add School Year</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="px-5">
          <div class="my-3">
            <label for="">School Year</label>
            <div class="input-group align-items-center">
              <input type="text" name="" value="" placeholder="Enter Year" class="form-control">
              <span class="mx-3">-</span>
              <input type="text" name="" value="" placeholder="Enter Year" class="form-control">
            </div>
          </div>
          <div class="my-3">
            <label for="">Period</label>
            <div class="input-group align-items-center">
              <select class="form-control" name="">
                <option value="">January</option>
                <option value="">February</option>
                <option value="">March</option>
                <option value="">April</option>
                <option value="">May</option>
                <option value="">June</option>
                <option value="">July</option>
                <option value="">August</option>
                <option value="">September</option>
                <option value="">October</option>
                <option value="">November</option>
                <option value="">December</option>
              </select>
              <span class="mx-3">-</span>
              <select class="form-control" name="">
                <option value="">January</option>
                <option value="">February</option>
                <option value="">March</option>
                <option value="">April</option>
                <option value="">May</option>
                <option value="">June</option>
                <option value="">July</option>
                <option value="">August</option>
                <option value="">September</option>
                <option value="">October</option>
                <option value="">November</option>
                <option value="">December</option>
              </select>
            </div>
          </div>
          <div class="d-flex flex-row justify-content-between mt-4 mb-2">
            <a href="#" class="btn btn-secondary bg-secondary col-lg-4 my-2" data-bs-dismiss="modal" aria-label="Close">Cancel</a>
            <a href="#" class="btn btn-success bg-success col-lg-4 my-2">Save</a>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<!-- Modal Edit Grade -->
<div class="modal fade" id="editSchoolyearModal" tabindex="-1" aria-labelledby="editSchoolyearModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit School Year</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="px-5">
          <div class="my-3">
            <label for="">School Year</label>
            <div class="input-group align-items-center">
              <input type="text" name="" value="2016" placeholder="Enter Year" class="form-control">
              <span class="mx-3">-</span>
              <input type="text" name="" value="2017" placeholder="Enter Year" class="form-control">
            </div>
          </div>
          <div class="my-3">
            <label for="">Period</label>
            <div class="input-group align-items-center">
              <select class="form-control" name="">
                <option value="">January</option>
                <option value="">February</option>
                <option value="">March</option>
                <option value="">April</option>
                <option value="">May</option>
                <option value="">June</option>
                <option value="" selected>July</option>
                <option value="">August</option>
                <option value="">September</option>
                <option value="">October</option>
                <option value="">November</option>
                <option value="">December</option>
              </select>
              <span class="mx-3">-</span>
              <select class="form-control" name="">
                <option value="">January</option>
                <option value="">February</option>
                <option value="">March</option>
                <option value="">April</option>
                <option value="">May</option>
                <option value="" selected>June</option>
                <option value="">July</option>
                <option value="">August</option>
                <option value="">September</option>
                <option value="">October</option>
                <option value="">November</option>
                <option value="">December</option>
              </select>
            </div>
          </div>
          <div class="d-flex flex-row justify-content-between mt-4 mb-2">
            <a href="#" class="btn btn-secondary bg-secondary col-lg-4 my-2" data-bs-dismiss="modal" aria-label="Close">Cancel</a>
            <a href="#" class="btn btn-success bg-success col-lg-4 my-2">Save</a>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

@endsection
@push('js')
  <script type="text/javascript">
      $('#admissionCollapse').addClass('collapse show');
  </script>
@endpush
