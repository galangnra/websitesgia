<div class="kingster-top-bar">
    <div class="kingster-top-bar-background"></div>
    <div class="kingster-top-bar-container kingster-container ">
        <div class="kingster-top-bar-container-inner clearfix">
            {{-- <div class="kingster-top-bar-left kingster-item-pdlr">ADMISSION HOTLINE <i class="fa fa-phone" id="i_fd84_1"></i>+6281999467333</div> --}}
            <div class="kingster-top-bar-right kingster-item-pdlr">
                <ul id="kingster-top-bar-menu" class="sf-menu kingster-top-bar-menu kingster-top-bar-right-menu">
                    <li class="menu-item kingster-normal-menu"><a href="https://globalia.managebac.com/login">Portal</a></li>
                    <li class="menu-item kingster-normal-menu"><a href="http://primarylibrary.sgiaedu.org/">Library</a></li>
                </ul>
                <div class="kingster-top-bar-right-social"></div><a class="kingster-top-bar-right-button" href="#" target="_blank">ADMISSION HOTLINE (+6281999467333)</a></div>
        </div>
    </div>
</div>
<header class="kingster-header-wrap kingster-header-style-plain  kingster-style-menu-right kingster-sticky-navigation kingster-style-fixed" data-navigation-offset="75px">
    <div class="kingster-header-background"></div>
    <div class="kingster-header-container  kingster-container">
        <div class="kingster-header-container-inner clearfix">
            <div class="kingster-logo  kingster-item-pdlr">
                <div class="kingster-logo-inner">
                    <a class="" href="{{route('dashboard')}}">
                        <img src="{{asset('assets/imgs/logo.webp')}}" alt="" width="35px" /> 
                        <span>SEKOLAH GLOBAL INDO-ASIA</span>
                    </a>
                </div>
            </div>
            <div class="kingster-navigation kingster-item-pdlr clearfix ">
                <div class="kingster-main-menu" id="kingster-main-menu">
                    <ul id="menu-main-navigation-1" class="sf-menu">
                        @foreach ($navbars as $navbar)
                            <li class="menu-item menu-item-home {{$navbar->children()->get()->count() > 0 ? 'menu-item-has-children' : ''}} kingster-normal-menu"><a href="{{route('page', $navbar->slug)}}" class="sf-with-ul-pre">{{ucfirst($navbar->title)}}</a>
                                <ul class="sub-menu">
                                    @if ($navbar->children()->get()->count() > 0)
                                        @foreach ($navbar->children()->get() as $navChild)                                            
                                            <li class="menu-item" data-size="60"><a href="{{route('page', $navChild->slug)}}">{{ucfirst($navChild->title)}}</a></li>
                                        @endforeach                                
                                    @endif
                                </ul>
                            </li>                            
                        @endforeach
                    </ul>
                    <div class="kingster-navigation-slide-bar" id="kingster-navigation-slide-bar"></div>
                </div>
            </div>
        </div>
    </div>
</header>