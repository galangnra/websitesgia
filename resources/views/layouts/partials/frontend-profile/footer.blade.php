<footer>            
    <div class="kingster-footer-wrapper ">
        <div class="kingster-footer-container kingster-container clearfix">
            {{-- <div class="kingster-footer-column kingster-item-pdlr kingster-column-30">
                <div class="kingster-footer-wrapper ">
                    <h3 class="kingster-widget-title">Accredited by:</h3><span class="clear"></span>
                    <div class="kingster-footer-container kingster-container clearfix">
                        <div class="kingster-footer-column kingster-item-pdlr kingster-column-20">
                            <img src="{{asset('assets/imgs/logo-white.png')}}" alt="" />
                        </div>
                        <div class="kingster-footer-column kingster-item-pdlr kingster-column-30">
                            <p>Jl. Ahmad Yani, Kav. SGIA
                                <br /> Batam Centre - 29461
                                <br /> Batam - Indonesia.</p>
                        </div>
                        <div class="kingster-footer-column kingster-item-pdlr kingster-column-30">
                            <p>
                                <span id="span_1dd7_11">+62 778 467 333</span>
                                <br /> <span id="span_1dd7_11">+62 778 467 979</span>
                                <br /> <span class="gdlr-core-space-shortcode" id="span_1dd7_12"></span>
                                <br /> <a id="a_1dd7_8" href="mailto:admission@sgiaedu.org">admission@sgiaedu.org</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div id="text-2" class="widget widget_text kingster-widget">
                    <div class="textwidget">
                        <p><img src="{{asset('assets/imgs/logo-white.png')}}" alt="" />
                            <br /> <span class="gdlr-core-space-shortcode" id="span_1dd7_10"></span>
                            <br /> Jl. Ahmad Yani, Kav. SGIA
                            <br /> Batam Centre - 29461
                            <br /> Batam - Indonesia.</p>
                        <p><span id="span_1dd7_11">+62 778 467 333</span>
                            <br /> <span id="span_1dd7_11">+62 778 467 979</span>
                            <br /> <span class="gdlr-core-space-shortcode" id="span_1dd7_12"></span>
                            <br /> <a id="a_1dd7_8" href="mailto:admission@sgiaedu.org">admission@sgiaedu.org</a></p>
                        <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-left-align">
                            <div class="gdlr-core-divider-line gdlr-core-skin-divider" id="div_1dd7_111"></div>
                        </div>
                    </div>
                </div>
            </div> --}}
            {{-- <div class="kingster-footer-column kingster-item-pdlr kingster-column-15">
                <div id="gdlr-core-custom-menu-widget-2" class="widget widget_gdlr-core-custom-menu-widget kingster-widget">
                    <div class="menu-our-campus-container">
                        <div>
                            <div class="gdlr-core-pbf-element">
                                <div class="gdlr-core-button-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align">
                                    <a class="gdlr-core-button  gdlr-core-button-solid gdlr-core-button-no-border" href="#" style="font-size: 14px ;font-weight: 700 ;letter-spacing: 0px ;padding: 13px 26px 16px 30px;text-transform: none ;margin: 0px 10px 10px 0px;border-radius: 2px;-moz-border-radius: 2px;-webkit-border-radius: 2px;background: #3db166 ;"><span class="gdlr-core-content">Teacher Login</span><i class="gdlr-core-pos-right fa fa-external-link" style="font-size: 14px ;"></i></a>

                                    <a class="gdlr-core-button  gdlr-core-button-solid gdlr-core-button-no-border" href="#" style="font-size: 14px ;font-weight: 700 ;letter-spacing: 0px ;padding: 13px 26px 16px 30px;text-transform: none ;margin: 0px 10px 10px 0px;border-radius: 2px;-moz-border-radius: 2px;-webkit-border-radius: 2px;background: #3db166 ;"><span class="gdlr-core-content">Student Login</span><i class="gdlr-core-pos-right fa fa-external-link" style="font-size: 14px ;"></i></a>

                                    <a class="gdlr-core-button  gdlr-core-button-solid gdlr-core-button-no-border" href="#" style="font-size: 14px ;font-weight: 700 ;letter-spacing: 0px ;padding: 13px 26px 16px 30px;text-transform: none ;margin: 0px 10px 10px 0px;border-radius: 2px;-moz-border-radius: 2px;-webkit-border-radius: 2px;background: #3db166 ;"><span class="gdlr-core-content">Parent Login</span><i class="gdlr-core-pos-right fa fa-external-link" style="font-size: 14px ;"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
            <div class="kingster-footer-column kingster-item-pdlr kingster-column-30">
                <div id="gdlr-core-custom-menu-widget-3" class="widget widget_gdlr-core-custom-menu-widget kingster-widget">
                    <h3 class="kingster-widget-title">SEKOLAH GLOBAL INDO-ASIA</h3><span class="clear"></span>
                    <div class="menu-campus-life-container">
                        <div class="gdlr-core-pbf-element">
                            <div class="gdlr-core-gallery-item gdlr-core-item-pdb clearfix  gdlr-core-gallery-item-style-grid" id="div_1dd7_110">
                                <div class="gdlr-core-gallery-item-holder gdlr-core-js-2 clearfix" data-layout="fitrows">
                                    <div class="kingster-footer-column kingster-item-pdlr kingster-column-30">
                                        <p>Jl. Ahmad Yani, Kav. SGIA
                                            <br /> Batam Centre - 29461
                                            <br /> Batam - Indonesia.</p>
                                    </div>
                                    <div class="kingster-footer-column kingster-item-pdlr kingster-column-30">
                                        <p>
                                            <span id="span_1dd7_11"><i class="fa fa-phone"></i> +62 778 467 333</span>
                                            <br /> <span id="span_1dd7_11"><i class="fa fa-fax"></i> +62 778 467 979</span>
                                            <br /> <a id="a_1dd7_8" href="mailto:admission@sgiaedu.org"><i class="fa fa-envelope-open-o"></i> admission@sgiaedu.org</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="gdlr-core-gallery-item gdlr-core-item-pdb clearfix  gdlr-core-gallery-item-style-grid" id="div_1dd7_110">
                                <div class="gdlr-core-gallery-item-holder gdlr-core-js-2 clearfix" data-layout="fitrows">
                                    <div class="kingster-footer-column kingster-item-pdlr kingster-column-20">
                                        <a class="gdlr-core-button  gdlr-core-button-solid gdlr-core-button-no-border" href="http://globalia.managebac.com/" style="font-size: 14px ;font-weight: 700 ;letter-spacing: 0px ;padding: 13px 26px 16px 30px;text-transform: none ;margin: 0px 10px 10px 0px;border-radius: 2px;-moz-border-radius: 2px;-webkit-border-radius: 2px;width:100%;"><span class="gdlr-core-content">Teacher Login</span></a>
                                    </div>
                                    <div class="kingster-footer-column kingster-item-pdlr kingster-column-20">
                                        <a class="gdlr-core-button  gdlr-core-button-solid gdlr-core-button-no-border" href="http://globalia.managebac.com/" style="font-size: 14px ;font-weight: 700 ;letter-spacing: 0px ;padding: 13px 26px 16px 30px;text-transform: none ;margin: 0px 10px 10px 0px;border-radius: 2px;-moz-border-radius: 2px;-webkit-border-radius: 2px;width:100%;"><span class="gdlr-core-content">Student Login</span></a>
                                    </div>
                                    <div class="kingster-footer-column kingster-item-pdlr kingster-column-20">
                                        <a class="gdlr-core-button  gdlr-core-button-solid gdlr-core-button-no-border" href="http://globalia.managebac.com/" style="font-size: 14px ;font-weight: 700 ;letter-spacing: 0px ;padding: 13px 26px 16px 30px;text-transform: none ;margin: 0px 10px 10px 0px;border-radius: 2px;-moz-border-radius: 2px;-webkit-border-radius: 2px;width:100%;"><span class="gdlr-core-content">Parent Login</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kingster-footer-column kingster-item-pdlr kingster-column-30">
                <div id="gdlr-core-custom-menu-widget-3" class="widget widget_gdlr-core-custom-menu-widget kingster-widget">
                    <h3 class="kingster-widget-title">Accredited by:</h3><span class="clear"></span>
                    <div class="menu-campus-life-container">
                        <div class="gdlr-core-pbf-element">
                            <div class="gdlr-core-gallery-item gdlr-core-item-pdb clearfix  gdlr-core-gallery-item-style-grid" id="div_1dd7_110">
                                <div class="gdlr-core-gallery-item-holder gdlr-core-js-2 clearfix" data-layout="fitrows">
                                    <div class="gdlr-core-item-list gdlr-core-gallery-column  gdlr-core-column-12 gdlr-core-column-first gdlr-core-item-pdlr gdlr-core-item-mgb">
                                        <div class="gdlr-core-gallery-list gdlr-core-media-image"><img src="{{ asset('assets/imgs/tut-wuri-handayani.webp') }}" alt="" title="banner-1" /></div>
                                    </div>
                                    <div class="gdlr-core-item-list gdlr-core-gallery-column  gdlr-core-column-12 gdlr-core-item-pdlr gdlr-core-item-mgb">
                                        <div class="gdlr-core-gallery-list gdlr-core-media-image"><img src="{{ asset('assets/imgs/ib.webp') }}" alt="" title="banner-2" /></div>
                                    </div>
                                    <div class="gdlr-core-item-list gdlr-core-gallery-column  gdlr-core-column-36 gdlr-core-item-pdlr gdlr-core-item-mgb">
                                        <div class="gdlr-core-gallery-list gdlr-core-media-image"><img src="{{ asset('assets/imgs/cambridge.webp') }}" alt="" title="banner-3" style="width: 80%;"/></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="kingster-copyright-wrapper">
        <div class="kingster-copyright-container kingster-container clearfix">
            <div class="kingster-copyright-left kingster-item-pdlr">Copyright All Right Reserved {{date('Y')}}, Sekolah Global Indo-Asia Batam</div>
            <div class="kingster-copyright-right kingster-item-pdlr">
                <div class="gdlr-core-social-network-item gdlr-core-item-pdb  gdlr-core-none-align" id="div_1dd7_112">
                    <a href="#" target="_blank" class="gdlr-core-social-network-icon" title="facebook">
                        <i class="fa fa-facebook" ></i>
                    </a>
                    <a href="#" target="_blank" class="gdlr-core-social-network-icon" title="google-plus">
                        <i class="fa fa-google-plus" ></i>
                    </a>
                    <a href="#" target="_blank" class="gdlr-core-social-network-icon" title="linkedin">
                        <i class="fa fa-linkedin" ></i>
                    </a>
                    <a href="#" target="_blank" class="gdlr-core-social-network-icon" title="skype">
                        <i class="fa fa-skype" ></i>
                    </a>
                    <a href="#" target="_blank" class="gdlr-core-social-network-icon" title="twitter">
                        <i class="fa fa-twitter" ></i>
                    </a>
                    <a href="#" target="_blank" class="gdlr-core-social-network-icon" title="instagram">
                        <i class="fa fa-instagram" ></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>