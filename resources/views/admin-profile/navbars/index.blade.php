@extends('adminlte::page')

@section('title', 'AdminSGIA - Navbar')
@section('plugins.Datatables', true)

{{-- Setup data for datatables --}}
@php
$heads = [
    'ID',
    'Title',
    ['label' => 'Positions', 'no-export' => true, 'width' => 5],
    ['label' => 'Actions', 'no-export' => true, 'width' => 5],
];
$data = array();
foreach ($navbars as $navbar) {
    $seqUp = (int)$navbar->sequence-1;
    $seqDown = (int)$navbar->sequence+1;
    $btnEdit = '<button class="btn btn-xs btn-default text-primary mx-1 shadow" title="Edit" data-toggle="modal" data-target="#modalEditNavbar'.$navbar->id.'">
                    <i class="fa fa-lg fa-fw fa-pen"></i>
                </button>';
    $btnDelete = '<button class="btn btn-xs btn-default text-danger mx-1 shadow" title="Delete">
                    <i class="fa fa-lg fa-fw fa-trash"></i>
                </button>';
    $btnDetails = '<a href="'.route('admin.navbars.show', $navbar).'" class="btn btn-xs btn-default text-teal mx-1 shadow"><i class="fa fa-lg fa-fw fa-eye"></i></a>';
    $btnMoveUp = '<button class="btn btn-xs btn-default text-teal mx-1 shadow btn-move-up" title="Details" data-sequence="'.$seqUp.'" data-navbar="'.$navbar->id.'">
                    <i class="fas fa-lg fa-fw fa-chevron-up"></i>
                </button>';
    $btnMoveDown = '<button class="btn btn-xs btn-default text-teal mx-1 shadow btn-move-down" title="Details" data-sequence="'.$seqDown.'" data-navbar="'.$navbar->id.'">
                    <i class="fas fa-lg fa-fw fa-chevron-down"></i>
                </button>';
    $formUpdateNavbar = '<form id="updateNavbar_'.$navbar->id.'" action='.route("admin.navbars.update", $navbar).' method="POST">
                        '.csrf_field().'
                        <input type="hidden" name="_method" value="PUT" />
                        <input type="hidden" name="sequence"/>
                        <input type="hidden" name="title" value="'.$navbar->title.'">
                    </form>';
    $modalEdit = '<div class="modal fade" id="modalEditNavbar'.$navbar->id.'" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
                        <div class="modal-content">
                            <div class="modal-header bg-teal">
                                <h4 class="modal-title">
                                    <i class="fas fa-plus mr-2"></i> Edit Navbar            
                                </h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <form action='.route("admin.navbars.update", $navbar).' method="POST" id="formEditNavbar'.$navbar->id.'">
                                <div class="modal-body">
                                    '.csrf_field().'
                                    <input type="hidden" name="_method" value="PUT" />
                                    <input type="hidden" name="sequence" value="'.$navbar->sequence.'">
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label for="title"> Title </label>
                                            <div class="input-group">
                                                <input id="title" name="title" class="form-control" placeholder="Enter Title" value="'.$navbar->title.'">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="modal-footer">
                                    <button type="submit" class="btn btn-success mr-auto submit-edit-navbar"> Save </button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal"> Discard </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>';
    $bar = array(
        $navbar->sequence, 
        $navbar->title,
        '<nobr>'.$btnMoveUp.$btnMoveDown.$formUpdateNavbar.'</nobr>',
        '<nobr>'.$btnEdit.$btnDelete.$btnDetails.$modalEdit.'</nobr>'
    );
    $data[] = $bar;
}


$config = [
    'data' => $data,
    'order' => [[1, 'asc']],
    'columns' => [null, null, ['orderable' => false]],
];
// dd($config['data']);
@endphp

@section('content_header')
    {{-- <h1>Navbars</h1> --}}
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Navbars</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Navbars</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
    @if (session('message'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert {{ session('class') }} alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('message') }}
                </div>
            </div>
        </div>
    @endif
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Navbar List</h3>
                    <div class="card-tools">
                        <x-adminlte-button label="Add Navbar" data-toggle="modal" data-target="#modalCreateNavbar" class="bg-teal"/>
                    </div>
                </div>
                <!-- ./card-header -->
                <div class="card-body">
                    <x-adminlte-datatable id="table1" :heads="$heads">
                        @foreach($config['data'] as $row)
                            <tr>
                                @foreach($row as $cell)
                                    <td>{!! $cell !!}</td>
                                @endforeach
                            </tr>
                        @endforeach
                    </x-adminlte-datatable>
                    {{-- Modal Create --}}
                    <x-adminlte-modal id="modalCreateNavbar" title="Add New Navbar" size="lg" theme="teal"
                    icon="fas fa-plus" v-centered static-backdrop scrollable>
                        <form action="{{ route('admin.navbars.store') }}" method="POST" id="formCreateNavbar">
                            {{ csrf_field() }}
                            <div class="row">
                                <x-adminlte-input name="title" label="Title" placeholder="Enter Title" fgroup-class="col-md-12" disable-feedback/>
                            </div>
                        </form>
                        <x-slot name="footerSlot">
                            <x-adminlte-button class="mr-auto submit-create-navbar" theme="success" type="submit" label="Save"/>
                            <x-adminlte-button theme="danger" label="Discard" data-dismiss="modal"/>
                        </x-slot>
                    </x-adminlte-modal>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
    <script>
        $(document).ready(function(){
            $(".submit-create-navbar").click(function(){        
                $("#formCreateNavbar").submit(); // Submit the form
            });
            $(".btn-move-up").click(function(){      
                var position = $(this).attr('data-sequence');
                var navbarId = $(this).attr('data-navbar');
                $("input[name='sequence']").val(position);
                $('#updateNavbar_'+navbarId).submit();
            });
            $(".btn-move-down").click(function(){      
                var position = $(this).attr('data-sequence');
                var navbarId = $(this).attr('data-navbar');
                console.log(navbarId, position);
                $("input[name='sequence']").val(position);
                $('#updateNavbar_'+navbarId).submit();
            });
        });
    </script>
@stop