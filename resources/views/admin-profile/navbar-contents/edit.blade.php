@extends('adminlte::page')

@section('title', 'AdminSGIA - Edit Navbar Content')
@section('plugins.Summernote', true)
@section('plugins.Select2', true)
{{-- @section('plugins.BsCustomFileInput', true) --}}

@section('content_header')
    {{-- <h1>Navbars</h1> --}}
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Edit Navbar Contents</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.navbars.index')}}">Navbars</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.navbars.contents.index')}}">Contents</a></li>
            <li class="breadcrumb-item active">Edit</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
    @if (session('message'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert {{ session('class') }} alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('message') }}
                </div>
            </div>
        </div>
    @endif
@stop

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('admin.navbars.contents.update', $navbarContent)}}" method="POST">
                    @csrf
                    @method('PUT')
                    {{-- Input Navbar --}}
                    <x-adminlte-select2 name="navbar_id" label="Navbar URL">
                        @foreach ($navbarUrls as $navbarUrl)
                            <option value="{{$navbarUrl->id}}" {{$navbarContent->navbar_id == $navbarUrl->id ? 'selected': ''}}>{{$navbarUrl->url}}</option>                            
                        @endforeach
                    </x-adminlte-select2>
                    {{-- Input Title --}}
                    <x-adminlte-input name="title" label="Title" placeholder="Enter Title" value="{{$navbarContent->title}}"/>
                    {{-- Input Content Link --}}
                    {{-- <x-adminlte-input name="content" label="Content" placeholder="Enter Link"/> --}}
                    {{-- Input Content Type --}}
                    <x-adminlte-select2 name="content_type" label="Content Type">
                        <option value="text" {{$navbarContent->content_type == 'text' ? 'selected': ''}}>Text</option>
                        <option value="image" {{$navbarContent->content_type == 'image' ? 'selected': ''}}>Image</option>
                        <option value="file" {{$navbarContent->content_type == 'file' ? 'selected': ''}}>File</option>
                        <option value="link" {{$navbarContent->content_type == 'link' ? 'selected': ''}}>Link</option>
                    </x-adminlte-select2>
                    {{-- Input Content Text --}}
                    <x-adminlte-text-editor label="Content" name="content">
                        {!!$navbarContent->content!!}
                    </x-adminlte-text-editor>
                    <div class="text-right">
                        <x-adminlte-button class="btn-flat" type="submit" label="Submit" theme="success" icon="fas fa-lg fa-save"/>
                    </div>
                </form> 
            </div>
        </div>
    </div>
</div>

@stop

@section('css')
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
    <script>
        $(document).ready(function(){
            $("select[name='content_type']").on('change', function() {
                console.log(this.value);
            });;
            
        });
    </script>
@stop