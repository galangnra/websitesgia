@extends('adminlte::page')

@section('title', 'AdminSGIA - Add Banners')
@section('plugins.BsCustomFileInput', true)

@section('content_header')
    {{-- <h1>Navbars</h1> --}}
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Add New Banner</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.banners.index')}}">Banner</a></li>
            <li class="breadcrumb-item active">Add New</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
    @if (session('message'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert {{ session('class') }} alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('message') }}
                </div>
            </div>
        </div>
    @endif
@stop

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('admin.banners.store')}}" enctype="multipart/form-data" method="POST" id="createBanners">
                    @csrf
                    {{-- With prepend slot, label and data-placeholder config --}}
                    <div class="row">
                        <div class="col-md-12">
                            <x-adminlte-input name="title" label="Title" placeholder="Title"
                                fgroup-class="col-md-12" disable-feedback/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <x-adminlte-input name="sequence" label="Sequence"  type="number" placeholder="Sequence"
                                fgroup-class="col-md-12" disable-feedback/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <x-adminlte-input-file name="image" id="uploadImage" accept="image/*" igroup-size="sm" placeholder="Choose a file..." fgroup-class="col-md-12">
                                <x-slot name="prependSlot">
                                    <div class="input-group-text bg-lightblue">
                                        <i class="fas fa-upload"></i>
                                    </div>
                                </x-slot>
                            </x-adminlte-input-file>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <img id="preview-image-before-upload" src="https://www.riobeauty.co.uk/images/product_image_not_found.gif"
                                alt="preview image" style="max-width: 1800px;max-height: 1119px;">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <x-adminlte-button class="btn-flat" style="" type="submit" label="Submit" theme="success" icon="fas fa-lg fa-save"/>
                        </div>
                    </div>
                </form> 
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
    <style>
       
    </style>
@stop
@section('js')
    <script>
        $(document).ready(async function(){
            $('#uploadImage').change(async function(){
                var img, file;
                let reader = new FileReader();
                reader.onload = async (e) => { 
                    await $('#preview-image-before-upload').attr('src', e.target.result); 
                    var image = $('#preview-image-before-upload')[0];
                    if(image.width == 1800 && image.height == 1119){
                    }else{
                        alert('Invalid Height or Width of selected image\nWidth : 1800\nHeight : 1119');
                        $('#preview-image-before-upload').attr('src', 'https://www.riobeauty.co.uk/images/product_image_not_found.gif')
                    }
                }
                await reader.readAsDataURL(this.files[0]);
            });
        });
    </script>
@stop
