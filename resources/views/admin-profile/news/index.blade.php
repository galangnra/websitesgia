@extends('adminlte::page')

@section('title', 'AdminSGIA - News')
@section('plugins.Datatables', true)

{{-- Setup data for datatables --}}
@php
$heads = [
    'ID',
    'Title',
    'Published at',
    ['label' => 'Actions', 'no-export' => true, 'width' => 5],
];
$data = array();
$i = 1;
foreach ($news as $newsS) {
    $btnEdit = '<a href="'.route('admin.news.edit', $newsS).'" class="btn btn-xs btn-default text-teal mx-1 shadow"><i class="fa fa-lg fa-fw fa-pen"></i></a>';
    $btnDelete = '<button class="btn btn-xs btn-default text-danger mx-1 shadow" title="Delete">
                    <i class="fa fa-lg fa-fw fa-trash"></i>
                </button>';
    $btnDetails = '<a href="'.route('admin.news.show', $newsS).'" class="btn btn-xs btn-default text-teal mx-1 shadow"><i class="fa fa-lg fa-fw fa-eye"></i></a>';
    
    
    $bar = array(
        $i,
        $newsS->title, 
        $newsS->published_at,
        '<nobr>'.$btnEdit.$btnDelete.$btnDetails.'</nobr>'
    );
    $data[] = $bar;
    $i += 1;
}


$config = [
    'data' => $data,
    'order' => [[1, 'asc']],
    'columns' => [null, null, ['orderable' => false]],
];
// dd($config['data']);
@endphp

@section('content_header')
    {{-- <h1>Navbars</h1> --}}
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">News</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">News</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
    @if (session('message'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert {{ session('class') }} alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('message') }}
                </div>
            </div>
        </div>
    @endif
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">News List</h3>
                    <div class="card-tools">
                        <a href="{{route('admin.news.create')}}" class="btn btn-default bg-teal">Add News</a>
                    </div>
                </div>
                <!-- ./card-header -->
                <div class="card-body">
                    <x-adminlte-datatable id="table1" :heads="$heads">
                        @foreach($config['data'] as $row)
                            <tr>
                                @foreach($row as $cell)
                                    <td>{!! $cell !!}</td>
                                @endforeach
                            </tr>
                        @endforeach
                    </x-adminlte-datatable>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
    <script>
        $(document).ready(function(){
            
        });
    </script>
@stop