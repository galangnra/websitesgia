@extends('adminlte::page')

@section('title', 'AdminSGIA - Edit News')
@section('plugins.Summernote', true)
@section('plugins.Select2', true)
@section('plugins.DateRangePicker', true)

@section('content_header')
    {{-- <h1>Navbars</h1> --}}
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Edit News</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.navbars.index')}}">Navbars</a></li>
            <li class="breadcrumb-item"><a href="{{route('admin.news.index')}}">Contents</a></li>
            <li class="breadcrumb-item active">Edit</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
    @if (session('message'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert {{ session('class') }} alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('message') }}
                </div>
            </div>
        </div>
    @endif
@stop

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('admin.news.update', $news)}}" method="POST">
                    @csrf
                    @method('PUT')
                    {{-- Input Title --}}
                    <x-adminlte-input name="title" label="Title" placeholder="Enter Title" value="{{$news->title}}"/>
                    {{-- Input Content Link --}}
                    {{-- <x-adminlte-input name="content" label="Content" placeholder="Enter Link"/> --}}
                    {{-- Input Content Type --}}
                    <x-adminlte-select2 name="content_type" label="Content Type">
                        <option value="text" {{$news->content_type == 'text' ? 'selected': ''}}>Text</option>
                        <option value="image" {{$news->content_type == 'image' ? 'selected': ''}}>Image</option>
                        <option value="file" {{$news->content_type == 'file' ? 'selected': ''}}>File</option>
                        <option value="link" {{$news->content_type == 'link' ? 'selected': ''}}>Link</option>
                    </x-adminlte-select2>
                    {{-- Input Content Text --}}
                    <x-adminlte-text-editor label="Content" name="content">
                        {!!$news->content!!}
                    </x-adminlte-text-editor>
                    @php
                    $config = [
                        "singleDatePicker" => true,
                        "showDropdowns" => true,
                        "startDate" => "js:moment()",
                        "minYear" => 2000,
                        "maxYear" => "js:parseInt(moment().format('YYYY'),10)",
                        "timePicker" => true,
                        "timePicker24Hour" => true,
                        "timePickerSeconds" => true,
                        "cancelButtonClasses" => "btn-danger",
                        "locale" => ["format" => "YYYY-MM-DD HH:mm:ss"],
                    ];
                    @endphp
                    <x-adminlte-date-range name="published_at" label="Publish at" :config="$config" value="{{$news->published_at}}">
                        <x-slot name="appendSlot">
                            <div class="input-group-text bg-dark">
                                <i class="fas fa-calendar-day"></i>
                            </div>
                        </x-slot>
                    </x-adminlte-date-range>
                    <div class="text-right">
                        <x-adminlte-button class="btn-flat" type="submit" label="Submit" theme="success" icon="fas fa-lg fa-save"/>
                    </div>
                </form> 
            </div>
        </div>
    </div>
</div>

@stop

@section('css')
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
@stop

@section('js')
    <script>
        $(document).ready(function(){
            $("select[name='content_type']").on('change', function() {
                console.log(this.value);
            });;
            
        });
    </script>
@stop