<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminProfile\NavbarController;
use App\Http\Controllers\AdminProfile\ContentController;
use App\Http\Controllers\AdminProfile\NavbarContentController;
use App\Http\Controllers\AdminProfile\NewsController;
use App\Http\Controllers\AdminProfile\BannerController;
use App\Http\Controllers\AdminProfile\UserController;
use App\Http\Controllers\AdminProfile\FiledownloadController;
use App\Http\Controllers\AdminProfile\DashboardController;
use App\Http\Controllers\FrontendProfile\PageController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });
Route::get('/', [HomeController::class, 'index'])->name('dashboard');
Route::get('/admin', [DashboardController::class, 'index'])->name('admin.dashboard');
Route::get('/page/{URI}', [PageController::class, 'show'])->name('page');

// Route::get('/admin2', function () {
//     return view('admin-profile.home');
// });

// Route for ADMIN PROFILE
Route::prefix('admin')->name('admin.')->group(function () {
    Route::resource('navbars', NavbarController::class);
    Route::resource('contents', ContentController::class);
    Route::prefix('navbar')->name('navbars.')->group(function () {
        Route::resource('contents', NavbarContentController::class);
    });
    Route::resource('news', NewsController::class);
    Route::resource('banners', BannerController::class);
    Route::resource('filedownloads', FiledownloadController::class);
    Route::resource('users', UserController::class);
});


// Route for ADMIN
// Route::prefix('admin')->name('admin.')->middleware('auth', 'verified', 'role:admin')->group(function () {
//     Route::get('/', function () {
//         return view('admin.index');
//     });
//     Route::get('/grade', function () {
//         return view('admin.grade.index');
//     });
//     Route::get('/school-years', function () {
//         return view('admin.grade.schoolYears');
//     });
//     Route::get('/nationality', function () {
//         return view('admin.grade.nationality');
//     });
    
//     Route::get('/addcandidate', function () {
//         return view('admin.students.addcandidate');
//     });
//     Route::get('/candidate', function () {
//         return view('admin.students.candidate');
//     });
//     Route::get('/editcandidate', function () {
//         return view('admin.students.editCandidate');
//     });
//     Route::get('/detailscandidate', function () {
//         return view('admin.students.detailsCandidate');
//     });  
    
    
//     Route::get('/student-information', function () {
//         return view('admin.students.studentInformation');
//     });
//     Route::get('/details-student-information', function () {
//         return view('admin.students.detailsStudentInformation');
//     });
// });


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

