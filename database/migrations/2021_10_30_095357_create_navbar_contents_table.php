<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNavbarContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('navbar_contents', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('title');
            $table->longText('slug');
            $table->enum('content_type', ['text', 'link', 'file', 'image']);
            $table->longText('content');
            $table->datetime('published_at')->nullable();
            $table->uuid('navbar_id')->unsigned();
            $table->boolean('is_active')->default(false);
            $table->timestamps();

            $table->foreign('navbar_id')
                ->references('id')->on('navbars')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('navbar_contents');
    }
}
